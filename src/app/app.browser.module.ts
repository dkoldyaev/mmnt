import { BrowserModule } from '@angular/platform-browser';
import {APP_ID, Inject, NgModule, PLATFORM_ID} from '@angular/core';

import {environment} from '../environments/environment';
import {ShareButtonsModule} from '@ngx-share/buttons';
import {MetrikaModule} from 'ng-yandex-metrika';

import 'hammerjs';
import 'hammer-timejs';
import {isPlatformBrowser} from '@angular/common';
import {SafeHtmlPipe} from './--pipes/safe-html/safe-html.pipe';
import {AppModule, Routing} from './app.module';
import {AppBrowserComponent} from './app.browser.component';
import {HttpClientJsonpModule, HttpClientModule} from '@angular/common/http';
import {NotFoundModule} from './not-found/not-found.module';
import {BannerModule} from './banner/banner.module';
import {SubscribeModule} from './subscribe/subscribe.module';
import {TagModule} from './tag/tag.module';
import {MenuModule} from './menu/menu.module';
import {PublicationPromoBlockModule} from './publication-promo-block/publication-promo-block.module';
import {EventsModule} from './events/events.module';
import {PublicationModule} from './publication/publication.module';
import {HomeModule} from './home/home.module';

const YandexMetrikaModule = MetrikaModule.forRoot({
  id: environment['yandexMetrikaId'] || '0',
  webvisor: true
});

@NgModule({
  declarations: [
    AppBrowserComponent,
  ],
  imports: [
    AppModule,
    BrowserModule.withServerTransition({ appId: 'momenty' }),
    ShareButtonsModule.forRoot(),
    YandexMetrikaModule,

    HomeModule,
    PublicationModule,
    EventsModule,
    PublicationPromoBlockModule,
    MenuModule,
    TagModule,
    SubscribeModule,
    BannerModule,
    NotFoundModule,

    HttpClientModule,
    HttpClientJsonpModule,
    ShareButtonsModule.forRoot(),

    Routing,
  ],
  bootstrap: [
    AppBrowserComponent
  ],
  providers: []
})
export class AppBrowserModule {

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string
  ) {
    const platform = isPlatformBrowser(platformId) ? 'in the browser' : 'on the server';
    // console.log(`Running ${platform} with appId=${appId}`);
  }

}
