import { TestBed, inject } from '@angular/core/testing';

import { PopularPublicationService } from './popular-publication.service';

describe('PopularPublicationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PopularPublicationService]
    });
  });

  it('should be created', inject([PopularPublicationService], (service: PopularPublicationService) => {
    expect(service).toBeTruthy();
  }));
});
