import { Component, OnInit } from '@angular/core';
import {PublicationDetailComponent} from '../publication-detail-component/publication-detail.component';
import {PublicationService} from '../publication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {PublicationPreviewService} from '../publication-preview.service';
import {RenderService} from '../render.service';
import {MetaService} from '../../--services/meta.service';
import {IsBrowserOrServerService} from '../../--services/is-browser-or-server.service';

@Component({
  selector: 'app-publication-preview',
  templateUrl: './../publication-detail-component/publication-detail.html',
  styleUrls: ['./../publication-detail-component/publication-detail.less'],
  providers: [
    PublicationPreviewService,
    RenderService,
  ],
})
export class PublicationPreviewComponent extends PublicationDetailComponent {

  constructor(
    protected publication_service: PublicationPreviewService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected meta_service: MetaService,
    protected redner_service: RenderService,
    public is_browser_or_server_service: IsBrowserOrServerService,
  ) {
    super(
      publication_service,
      route,
      router,
      meta_service,
      redner_service,
      is_browser_or_server_service
    );
  }

  tryIsExsist() {}

}
