import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicationPreviewComponent } from './publication-preview.component';

describe('PublicationPreviewComponent', () => {
  let component: PublicationPreviewComponent;
  let fixture: ComponentFixture<PublicationPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicationPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicationPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
