import { TestBed, inject } from '@angular/core/testing';

import { PublicationPreviewService } from './publication-preview.service';

describe('PublicationPreviewService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PublicationPreviewService]
    });
  });

  it('should be created', inject([PublicationPreviewService], (service: PublicationPreviewService) => {
    expect(service).toBeTruthy();
  }));
});
