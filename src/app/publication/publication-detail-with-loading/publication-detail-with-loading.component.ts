import {Component, ElementRef, HostListener, Input, OnDestroy, OnInit, QueryList, ViewChildren, ViewContainerRef} from '@angular/core';
import {DateListService} from '../../--abstract/--date-list-service';
import {IPublication, Publication} from '../publication';
import {PublicationService} from '../publication.service';
import {DateListComponent} from '../../--abstract/--date-list-component';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {Location} from '@angular/common';
import {PublicationDetailComponent} from '../publication-detail-component/publication-detail.component';
import {MetaService} from '../../--services/meta.service';
import {IsBrowserOrServerService} from '../../--services/is-browser-or-server.service';

@Component({
  selector: 'app-publication-detail-with-loading',
  templateUrl: './publication-detail-with-loading.component.html',
  styleUrls: [
    './publication-detail-with-loading.component.less',
  ],
  providers: [PublicationService]
})
export class PublicationDetailWithLoadingComponent implements OnInit, OnDestroy {

  private subs: Subscription;
  public id: number;
  public rubric_slug: string;
  public publications: Publication[] = [];
  public first_publication: Publication;
  public page_size = 1;
  private change_url_without_loading: boolean = false;
  public current_component_number: number = 0;

  private scroll_down: boolean;
  private scroll_up: boolean;
  private current_scroll_position: number;

  @Input()
  public loading_limit: number = 3;

  @ViewChildren(PublicationDetailComponent)
  publications_components: QueryList<PublicationDetailComponent>;

  @ViewChildren(PublicationDetailComponent, {read: ElementRef})
  publications_elements: QueryList<ElementRef>;

  constructor(
    protected service: PublicationService,
    protected route: ActivatedRoute,
    protected router: Router,
    private meta_service: MetaService,
    public is_browser_or_server_service: IsBrowserOrServerService,
  ) {
  }

  ngOnInit() {
    this.subs = this.route.params.subscribe(params => {
      this.publications = [];
      this.loadDetail(+params['id']);
      this.rubric_slug = params['rubric_slug'];
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  loadDetail(publication_id: number, index: number = 0) {
    // Загружаем одну текущую публикацию
    const detail_observe = this.service.getDetail(publication_id);
    detail_observe.subscribe(
      (data: IPublication) => {
        let new_publication = new Publication(data);

        // Если это первая публикация в подгрузке и это спецпроект, то редиректим на страницу спецпроекта
        if (new_publication.isOuterLink() && !this.is_browser_or_server_service.is_server && index === 0) {
          window.location.replace(new_publication.spec_link);
        }

        // Если публикация не первая, но спецпроект, то просто пытаемся загрузить следующую
        if (new_publication.isOuterLink() && !this.is_browser_or_server_service.is_server && index > 0) {
          if (new_publication.next_publication_id) {
            this.loadDetail(new_publication.next_publication_id, index);
          }
          return;
        }
        this.publications[index] = new_publication;
        if (this.publications.length === 1) {
          if (this.is_browser_or_server_service.is_browser) {
            window.scrollTo(0, 0);
          }
        }
        // Если это первая публикация, то обновляем мета-данные при ее загрузке.
        // Если нет, то этим будет заниматься штука, отслеживающая скролл
        if (index === 0) {
          if (new_publication.primary_rubric.slug !== this.rubric_slug) {
            this.router.navigate(['/404']);
          }
          setTimeout(() => {
            this.changePublication(new_publication);
            this.publications_components.toArray()[0].show_share_buttons = this.publications.length === 1;
          });
        }
        if (this.is_browser_or_server_service.is_browser) {
          window.dispatchEvent(new Event('resize'));
        }
      },
      (error) => {
        this.router.navigate(['/404']);
      }
    );
  }

  getNextPublication() {
    // Если первая публикация еще не загрузилась, то скролл не отслеживаем
    if (this.publications.length === 0) {
      return;
    }
    if (this.loading_limit && this.publications.length >= this.loading_limit) {
      return;
    }
    if (this.publications[this.publications.length - 1]) {
      let next_publication_id = this.publications[this.publications.length - 1].next_publication_id;
      if (next_publication_id) {
        this.loadDetail(next_publication_id, this.publications.length);
      }
    }
  }

  @HostListener('window:scroll', [])
  detectCurrentPublication() {
    if (this.is_browser_or_server_service.is_server) {
      return;
    }
    this.scroll_down = this.current_scroll_position < window.scrollY;
    this.scroll_up = !this.scroll_down;
    this.current_scroll_position = window.scrollY;

    let window_top: number = window.scrollY;
    let window_bottom: number = window_top + window.screen.height;
    let count_active_elements: number = 0;
    let current_components = [];
    let current_component_els = [];
    let current_components_indexes: number[] = [];
    let new_publication_number = null;

    for (let publication_component_i in this.publications_elements.toArray()) {
      let component = this.publications_components.toArray()[publication_component_i];
      let component_el = this.publications_elements.toArray()[publication_component_i].nativeElement;
      let component_bottom: number = component_el.offsetTop + component_el.offsetHeight;
      let component_top: number = component_el.offsetTop;
      if (component_top <= window_bottom && component_bottom >= window_top) {
        count_active_elements++;
        current_components.push(component);
        current_components_indexes.push(+publication_component_i);
      }
    }

    if (this.scroll_up) {
      new_publication_number = current_components_indexes[current_components_indexes.length - 1];
    } else if (current_components_indexes.length === 1) {
      new_publication_number = current_components_indexes[0];
    }

    if (new_publication_number !== null &&
        new_publication_number !== this.current_component_number &&
        this.publications_components.toArray()[new_publication_number]) {
      this.current_component_number = new_publication_number;
      setTimeout(() => {
        this.changePublication(this.publications_components.toArray()[new_publication_number].current_publication);
        this.publications_components.toArray()[new_publication_number].show_share_buttons = true;
      });
    }

  }

  changePublication(publication: Publication) {
    if (!publication) {
      return;
    }

    this.meta_service.onMetaChanged.emit(publication);

    if (this.is_browser_or_server_service.is_browser) {

      let new_url = this.router.createUrlTree([
        publication.primary_rubric.slug,
        publication.id,
      ]).toString();

      if (window.location.pathname !== new_url) {
        window.history.replaceState(
          null,
          publication.title,
          new_url
        );
      }

    }
  }

  showed_publication_ids(): number[] {
    return this.publications.map((publication) => {
      return publication.id;
    });
  }

}
