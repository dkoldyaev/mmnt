import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicationDetailWithLoadingComponent } from './publication-detail-with-loading.component';

describe('PublicationDetailWithLoadingComponent', () => {
  let component: PublicationDetailWithLoadingComponent;
  let fixture: ComponentFixture<PublicationDetailWithLoadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicationDetailWithLoadingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicationDetailWithLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
