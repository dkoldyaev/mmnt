import { Injectable } from '@angular/core';
import {PublicationService} from './publication.service';
import {Headers, RequestOptions, URLSearchParams} from '@angular/http';

@Injectable()
export class PublicationPreviewService extends PublicationService {

  public get_service_name() {
    return 'publication_preview';
  }

  public applyFilter() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let params = new URLSearchParams();
    for (let filter_name in this.filters) {
      params.append(filter_name, this.filters[filter_name]);
    }
    params.append('preview', '1');

    this.options = new RequestOptions({
      headers: headers,
      params: params,
    });
  }

}
