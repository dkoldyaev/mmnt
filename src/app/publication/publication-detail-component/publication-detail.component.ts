import {
  AfterViewInit,
  Compiler, Component, ElementRef, HostListener, Input, ModuleWithComponentFactories, OnDestroy, OnInit, ViewChild,
  ViewContainerRef
} from '@angular/core';
import {PublicationService} from '../publication.service';
import {Publication} from '../publication';
import {Response} from '@angular/http';
import {ActivatedRoute, Router} from '@angular/router';
import {RenderService} from '../render.service';
import {MetaService} from '../../--services/meta.service';
import {DynamicContent2Component} from '../../dynamic-content-module/dynamic-content/dynamic-content.component';
import {IsBrowserOrServerService} from '../../--services/is-browser-or-server.service';
// import {ResizedEvent} from 'angular-resize-event/dist/resized-event';

@Component({
  moduleId: module.id,
  selector: 'app-publication-detail',
  providers: [
    PublicationService,
    RenderService,
  ],
  styleUrls: [
    './publication-detail.less'
  ],
  templateUrl: './publication-detail.html'
})
export class PublicationDetailComponent implements OnInit, OnDestroy {

  private subs: any;
  public id: number;
  public rubric_slug: string;

  @Input()
  public current_publication: Publication;

  @Input()
  public other_publications_count: number = 0;

  @Input()
  public show_right_banner: boolean = true;

  @Input()
  public show_share_buttons: boolean = true;

  public show_right_col: boolean = true;

  @ViewChild(DynamicContent2Component)
  dynamic_content_components;

  @ViewChild('contentBlock')
  contentBlock: ElementRef;

  constructor(
    protected publication_service: PublicationService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected meta_service: MetaService,
    protected render_service: RenderService,
    public is_browser_or_server_service: IsBrowserOrServerService,
  ) {}

  ngOnInit() {
    if (!this.current_publication) {
      this.subs = this.route.params.subscribe(params => {
        this.id = +(params['id']);
        this.rubric_slug = params['rubric_slug'];

        this.showDetail(this.id);
      });
    } else if (this.current_publication.text) {
      this.render_service.elements_count = this.current_publication.text.length;
    }

    this.render_service.all_elements_ready.subscribe((data) => {
      if (data) {
        setTimeout(() => {
          this.tryContentHeight();
        }, 300);
      }
    });
  }

  ngOnDestroy() {
    if (!this.current_publication) {
      this.subs.unsubscribe();
    }
  }

  tryIsExsist() {
    if (this.current_publication.primary_rubric.slug !== this.rubric_slug && this.current_publication.id === this.id) {
      this.router.navigate(['/404']);
    }
  }

  showDetail(publication_id: number) {
    const detail_stream = this.publication_service.getDetail(publication_id);
    detail_stream.subscribe(
      (data) => {
        this.current_publication = new Publication(data);
        this.tryIsExsist();
        if (this.dynamic_content_components) {
          this.dynamic_content_components.renderPublication(this.current_publication.text);
        }
        this.tryContentHeight();
        if (this.is_browser_or_server_service.is_browser) {
          window.scrollTo(0, 0);
        }
        if (this.current_publication.text) {
          this.render_service.elements_count = this.current_publication.text.length;
        }
      },
      (error) => {
        this.router.navigate(['/404']);
      }
    );
  }

  onResize(event) {
    this.tryContentHeight();
  }

  tryContentHeight() {
    this.show_right_banner = false;
    this.other_publications_count = 0;
    if (!this.contentBlock) {
      return;
    }
    let contentHeight: number = this.contentBlock.nativeElement.scrollHeight;
    if (contentHeight >= 650) {
      this.show_right_banner = true;
      this.other_publications_count = Math.min(
        Math.floor((contentHeight - 710 /*банер в пр. колонке*/ - 60 /*заголовок "гл.новости"*/) / 215),
        3
      );
    }
    this.show_right_col = this.other_publications_count > 0 || this.show_right_banner;
  }

  getPublicationUrl(absolute: boolean = false): string {
    let result = this.router.createUrlTree([
      this.current_publication.primary_rubric.slug,
      this.current_publication.id,
    ]).toString();

    if (this.is_browser_or_server_service.is_browser) {
      if (absolute) {
        result = window.location.protocol + '//' + window.location.host + result;
      }
    }

    return result;
  }

  isCurrentPublication(): boolean {
    return this.is_browser_or_server_service.is_server
      ? true
      : window.location.pathname === this.getPublicationUrl();
  }

}
