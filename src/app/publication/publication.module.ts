import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {PublicationDetailComponent} from './publication-detail-component/publication-detail.component';
import {NguCarouselModule} from '@ngu/carousel';
import {PublicationService} from './publication.service';
import {NgPipesModule} from 'ngx-pipes';
import { PublicationDetailWithLoadingComponent } from './publication-detail-with-loading/publication-detail-with-loading.component';
import {PublicationAnnounceModule} from '../publication-announce/publication-announce.module';
import {PublicationListModule} from '../publication-list/publication-list.module';
import {AdditionColumnModule} from '../addition-column/addition-column.module';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {SimpleElementsModule} from '../simple-elements/simple-elements.module';
import {ShareModule} from '../share/share.module';
import {AuthorModule} from '../author/author.module';
import { PublicationPreviewComponent } from './publication-preview/publication-preview.component';
import {DynamicContentModule} from '../dynamic-content-module/dynamic-content-module.module';
import {DynamicContent2Component} from '../dynamic-content-module/dynamic-content/dynamic-content.component';
// import {AngularResizedEventModule} from 'angular-resize-event/dist';

const publicationRoutes: Routes = [
  { path: 'publications/:rubric_slug/:id', component: PublicationDetailWithLoadingComponent, pathMatch: 'full'},
];

@NgModule({
  declarations: [
    PublicationDetailComponent,
    PublicationDetailWithLoadingComponent,
    PublicationPreviewComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(publicationRoutes),
    // TODO: Заменить карусель на ту, что используется в галере
    NguCarouselModule,
    NgPipesModule,
    InfiniteScrollModule,
    PublicationAnnounceModule,
    PublicationListModule,
    AdditionColumnModule,
    SimpleElementsModule,
    ShareModule,
    AuthorModule,
    DynamicContentModule,
  ],
  exports: [
  ],
  providers: [
    PublicationService
  ]
})
export class PublicationModule {

}
