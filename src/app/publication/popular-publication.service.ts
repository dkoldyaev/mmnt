import { Injectable } from '@angular/core';
import {ApiListService} from '../--abstract/--api-list-service';

@Injectable()
export class PopularPublicationService extends ApiListService {

  public get_service_name() {
    return 'popular_publication';
  }

}
