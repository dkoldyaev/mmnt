import * as moment from 'moment';
import {IRubric, Rubric} from '../rubric/rubric';
import {DynamicInterface} from '../dynamic-content-module/dynamic.interface';
import {IIMage, IMage} from '../simple-elements/image/image';
import {Author, IAuthor} from '../author/author';
import {IMetaObject, MetaObject} from '../--classes/meta-object';
import {Router} from '@angular/router';

export interface IPublication extends IMetaObject {

  id: number;

  title: string;
  subject: string;
  publish_at: Date|string;

  announce: string;
  announce_image: IMage;
  announce_image_type: string;

  authors: IAuthor[];

  text?: DynamicInterface[];
  rubrics: IRubric[];
  primary_rubric: IRubric;
  format: string;

  promo_image: IIMage;

  main_item_priority?: number;
  next_publication_id?: number;

  resource_uri: string;

  main_block_image: IIMage;
  main_block_custom_title: string;

  spec_link?: string;

}

export class Publication extends MetaObject implements IPublication {

  id: number;

  title: string;
  subject: string;
  publish_at: Date|string;

  announce: string;
  announce_image: IMage;
  announce_image_type: string;

  authors: Author[];

  text: DynamicInterface[];
  rubrics: Rubric[] = [];
  primary_rubric: Rubric;
  format: string;

  promo_image: IMage;

  main_item_priority?: number;
  next_publication_id?: number;

  resource_uri: string;

  main_block_image: IMage;
  main_block_custom_title: string;

  spec_link?: string;

  send_to_ura_ru: boolean;

  public isOuterLink(): boolean {
    return !!('spec_link' in this && this.spec_link);
  }

  public isDetail(): boolean {
    return !!this.text;
  }

  constructor(obj: IPublication) {

    super(obj);

    for (let property_key in obj) {

      let property_value = obj[property_key];

      switch (property_key) {

        case 'promo_image' :
        case 'announce_image' :
        case 'main_block_image' :
          property_value = new IMage(obj[property_key]);
          break;

        case 'rubrics' :
          property_value = [];
          for (let rubric_i in obj[property_key]) {
            if (obj[property_key][rubric_i]) {
              property_value.push(new Rubric(obj[property_key][rubric_i]));
            }
          }
          break;

        case 'primary_rubric' :
          if (obj[property_key]) {
            property_value = new Rubric(obj[property_key]);
          }
          break;

        case 'publish_at' :
          if (typeof property_value === 'string') {
            property_value = moment(property_value, 'YYYY-MM-DD HH:mm:ss').toDate();
          }
          break;

        case 'authors' :
          try {
            property_value = obj[property_key].map((author_data: IAuthor) => {
              return new Author(author_data);
            });
          } catch (e) {
            property_value = null;
          }
          break;

        case 'meta_description' :
          if (!obj[property_key]) {
            property_value = ' ';
          } else {
            property_value = obj[property_key];
          }

      }

      try {
        this[property_key] = property_value.trim();
      } catch (e) {
        // console.log(e);
      }

    }

    if (this.isDetail()) {
      let is_event_list = this.text.filter((dynamic_data) => {
        return dynamic_data.type === 'event-list';
      }).length > 0;

      if (is_event_list) {
        this.format = 'shedule';
      }
    }

  }

  toMetaDefinition() {
    let result = super.toMetaDefinition();
    result['name="mediator"'] = [{
      name: 'mediator',
      content: 'publication_' + this.id
    }];
    result['name="mediator_theme"'] = [{
      name: 'mediator_theme',
      content: this.primary_rubric.title
    }];
    for (let author_i in this.authors) {
      if (!result['name="mediator_author"']) {
        result['name="mediator_author"'] = [];
      }
      result['name="mediator_author"'].push({
        name: 'mediator_author',
        content: this.authors[author_i].first_name + ' ' + this.authors[author_i].last_name
      });
    }
    result['name="mediator_published_time"'] = [{
      name: 'mediator_published_time',
      content: moment(this.publish_at).format('YYYY-MM-DD\THH:mm:ss+0500')
    }];

    return result;
  }

}
