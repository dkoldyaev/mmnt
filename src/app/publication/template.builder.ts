import {Injectable} from '@angular/core';

@Injectable()
export class PublicationTemplateBuilder {

  public prepareTemplate(entity: any[]) {
    let result_template = '';

    entity.forEach((part, part_i, full_entity) => {
      result_template += `
        <app-template-${part['type']}
          [data]="part"
          [data_i]="part_i"
          [entity]="full_entity"></app-template-${part['type']}>
        `;
    });

    return result_template;
  }

}
