import {EventEmitter, Injectable} from '@angular/core';

@Injectable()
export class RenderService {

  elements_count: number = null;
  rendered_count: number = 0;

  element_ready: EventEmitter<boolean> = new EventEmitter<boolean>();
  all_elements_ready: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {
    this.element_ready.subscribe((data) => {
      this.rendered_count++;
      if (this.elements_count !== null && this.rendered_count === this.elements_count) {
        this.all_elements_ready.emit(true);
      }
    });
  }

}
