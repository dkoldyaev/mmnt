import {Injectable} from '@angular/core';
import {IPublication, Publication} from './publication';
import {DateListService} from '../--abstract/--date-list-service';
import {environment} from '../../environments/environment';

@Injectable()
export class PublicationService extends DateListService<Publication> {

  public get_service_name() {
    return 'publication';
  }

}
