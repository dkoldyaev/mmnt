import {Component, Input, OnInit} from '@angular/core';
import {PublicationListComponent} from '../publication-list-component/publication-list.component';
import {Publication} from '../../publication/publication';

@Component({
  selector: 'app-publication-related-list',
  templateUrl: './publication-related-list.component.html',
  styleUrls: ['./publication-related-list.component.less']
})
export class PublicationRelatedListComponent extends PublicationListComponent implements OnInit {

  @Input()
  exclude_publication_ids: number[] = [];

  public local_storage_variable = 'publications_showed_in_right_col';
  public local_storage_timer = 'publications_showed_date';
  public local_storage_limit = 60 * 60 * 1000; // один час

  public localStorageIsActual(): boolean {
    if (this.is_browser_or_server_service.is_browser) {
      let local_storage_time = +window.localStorage.getItem(this.local_storage_timer);
      if (!local_storage_time) {
        return true;
      } else if (+new Date() - local_storage_time > this.local_storage_limit) {
        return false;
      } else {
        return true;
      }
    }
    return false;
  }

  ngOnInit() {
    if (this.is_browser_or_server_service.is_browser && !this.localStorageIsActual()) {
      this.clearShowedPublications();
      window.localStorage.setItem(this.local_storage_timer, (+new Date()).toString());
    }
    this.exclude_publication_ids = this.exclude_publication_ids.concat(this.getShowedPublicationIds());
    super.ngOnInit();
  }

  public getShowedPublicationIds(): number[] {
    let showed_publciation_ids = [];
    return showed_publciation_ids;
  }

  public addShowedPublications(publications: Publication[]): void {
    let publication_ids_showed_in_right_col = this.getShowedPublicationIds().concat(publications.map((publication) => {
      return publication.id;
    }));
    window.localStorage.setItem(this.local_storage_variable, JSON.stringify(publication_ids_showed_in_right_col));
  }

  public isAlreadyShowed(publication: Publication): boolean {
    let showed_publications = this.getShowedPublicationIds();
    for (let i in showed_publications) {
      if (showed_publications[i] === publication.id) {
        return true;
      }
    }
    return false;
  }

  public clearShowedPublications() {
    if (this.is_browser_or_server_service.is_browser) {
      window.localStorage.removeItem(this.local_storage_variable);
    }
  }

  public filterQueue(): Publication[] {
    return this.queue.filter((publication) => {
      return this.exclude_publication_ids.indexOf(publication.id) < 0 && !this.isAlreadyShowed(publication);
    });
  }

  public filterElements(object: Publication): boolean {
    return this.exclude_publication_ids.indexOf(object.id) < 0;
  }

  newPageLoaded() {
    this.addShowedPublications(this.object_list);
  }

}
