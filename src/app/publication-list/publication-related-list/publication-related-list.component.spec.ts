import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicationRelatedListComponent } from './publication-related-list.component';

describe('PublicationRelatedListComponent', () => {
  let component: PublicationRelatedListComponent;
  let fixture: ComponentFixture<PublicationRelatedListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicationRelatedListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicationRelatedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
