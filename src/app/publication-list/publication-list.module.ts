import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PublicationListComponent} from './publication-list-component/publication-list.component';
import {PublicationRelatedListComponent} from './publication-related-list/publication-related-list.component';
import {PublicationRubricListComponent} from './publication-rubric-list/publication-rubric-list.component';
import {PublicationAnnounceModule} from '../publication-announce/publication-announce.module';
import {RouterModule, Routes} from '@angular/router';
import { PublicationPopularListComponent } from './publication-popular-list/publication-popular-list.component';
import {StickyModule} from 'ng2-sticky-kit';
import {SlickModule} from 'ngx-slick';
import {BannerModule} from '../banner/banner.module';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {ColumnsGridListModule} from '../columns-grid-list/columns-grid-list.module';

export const publicationListRoute: Routes = [
  { path: 'publications/:rubric_slug', component: PublicationRubricListComponent, pathMatch: 'full'},
  { path: 'publications', component: PublicationListComponent, pathMatch: 'full'},
];

@NgModule({
  imports: [
    CommonModule,
    PublicationAnnounceModule,
    RouterModule.forChild(publicationListRoute),
    StickyModule,
    SlickModule,
    BannerModule,
    InfiniteScrollModule,
    ColumnsGridListModule,
  ],
  exports: [
    PublicationListComponent,
    PublicationRelatedListComponent,
    PublicationRubricListComponent,
    PublicationPopularListComponent,
  ],
  declarations: [
    PublicationListComponent,
    PublicationRelatedListComponent,
    PublicationRubricListComponent,
    PublicationPopularListComponent,
  ]
})
export class PublicationListModule { }
