import {ApplicationRef, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {PublicationService} from '../../publication/publication.service';
import {Publication} from '../../publication/publication';
import {Response} from '@angular/http';
import {ActivatedRoute} from '@angular/router';
import {IRubric, Rubric} from '../../rubric/rubric';
import {RubricService} from '../../rubric/rubric.service';
import {Meta, Title} from '@angular/platform-browser';
import {DateListComponent} from '../../--abstract/--date-list-component';
import {DatePipe} from '@angular/common';
import * as moment from 'moment';
import {IsBrowserOrServerService} from '../../--services/is-browser-or-server.service';

@Component({
  moduleId: module.id,
  selector: 'app-publication-list',
  providers: [
    PublicationService,
    RubricService,
  ],
  styleUrls: [],
  template: ' '
})
export class PublicationListComponent extends DateListComponent<Publication, PublicationService> implements OnInit {

  @Input()
  public page_size = 10;

  @Input()
  public filters: any[] = [];

  protected subs: any;
  public current_publication: Publication = null;

  constructor(
    protected service: PublicationService,
    public is_browser_or_server_service: IsBrowserOrServerService,
  ) {
    super(service, is_browser_or_server_service, Publication);
  }

  ngOnInit() {
    this.getList();
  }

}
