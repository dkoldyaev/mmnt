import {Component, Input, OnInit} from '@angular/core';
import {PublicationListComponent} from '../publication-list-component/publication-list.component';
import {PublicationService} from '../../publication/publication.service';
import {PopularPublicationService} from '../../publication/popular-publication.service';
import {Publication} from '../../publication/publication';
import {IDataList} from '../../--abstract/--date-list';

declare const jquery: any;   // not required
declare const $: any;   // not required

@Component({
  selector: 'app-publication-popular-list',
  templateUrl: './publication-popular-list.component.html',
  styleUrls: ['./publication-popular-list.component.less'],
  providers: [PopularPublicationService]
})
export class PublicationPopularListComponent implements OnInit {

  object_list: Publication[] = [];

  @Input()
  exclude_ids: number[] = [];

  @Input()
  limit: number = 6;

  @Input()
  show_title: boolean = true;
  @Input()
  list_title: string = 'Популярное';

  constructor(protected service: PopularPublicationService) {
  }

  ngOnInit() {
    this.service._getList().subscribe((data: IDataList<Publication>) => {
      for (let publication_i in data.objects) {
        let new_publication = new Publication(data.objects[publication_i]);
        if (this.object_list.length >= this.limit) {
          break;
        }
        if (this.exclude_ids.indexOf(new_publication.id) < 0) {
          this.object_list.push(new_publication);
        }
      }
    });
  }

}
