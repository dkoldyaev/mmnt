import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicationPopularListComponent } from './publication-popular-list.component';

describe('PublicationPopularListComponent', () => {
  let component: PublicationPopularListComponent;
  let fixture: ComponentFixture<PublicationPopularListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicationPopularListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicationPopularListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
