import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {PublicationListComponent} from '../publication-list-component/publication-list.component';
import {IRubric, Rubric} from '../../rubric/rubric';
import {PublicationService} from '../../publication/publication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {RubricService} from '../../rubric/rubric.service';
import {MetaService} from '../../--services/meta.service';
import {MetaObject} from '../../--classes/meta-object';
import moment = require('moment');
import {IsBrowserOrServerService} from '../../--services/is-browser-or-server.service';

@Component({
  selector: 'app-publication-rubric-list',
  templateUrl: './publication-rubric-list.component.html',
  styleUrls: [
    './../publication-list-component/publication-list.component.less',
    './publication-rubric-list.component.less',
  ],
  providers: [
    PublicationService,
    RubricService,
  ]
})
export class PublicationRubricListComponent extends PublicationListComponent implements OnInit, OnDestroy {

  @Input()
  rubric: Rubric;

  @Input()
  columns_count: number = 4;

  @Input()
  columns: number[] = [0, 1, 2, 3];

  public page_size: number = 20;

  constructor(
    protected service: PublicationService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected rubric_service: RubricService,
    private meta_service: MetaService,
    public is_browser_or_server_service: IsBrowserOrServerService,
  ) {
    super(service, is_browser_or_server_service);
  }

  ngOnInit() {
    if (this.is_browser_or_server_service.is_browser) {
      window.scrollTo(0, 0);
    }
    this.subs = this.route.params.subscribe(params => {
      this.resetList();
      this.filters['rubric'] = params['rubric_slug'];
      this.service.setFilters(this.filters);
      this.getList();
      this.rubric_service.getDetail(params['rubric_slug']).subscribe(
        (data: IRubric) => {
          this.rubric = new Rubric(data);
          this.meta_service.onMetaChanged.emit(this.rubric);
        },
      (error) => {
          this.router.navigate(['/404']);
        }
      );
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  onResize() {
    let new_columns_count = this.is_browser_or_server_service.is_server
      ? 4
      : this.calcColumnsCount(window.innerWidth, 240, 20);
    if (new_columns_count !== this.columns_count) {
      this.columns_count = new_columns_count;
    }
  }

  calcColumnsCount(container_width: number, column_width: number, column_margin: number): number {
    let result = Math.floor(
      (container_width - column_margin) / (column_width + column_margin)
    );
    if (result > 4) {
      result = 4;
    }
    return result;
  }

  onScrollDown() {
    this.nextPage();
  }

  newPageLoaded() {
    this.onResize();
  }

}
