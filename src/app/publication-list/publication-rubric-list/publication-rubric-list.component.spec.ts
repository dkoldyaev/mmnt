import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicationRubricListComponent } from './publication-rubric-list.component';

describe('PublicationRubricListComponent', () => {
  let component: PublicationRubricListComponent;
  let fixture: ComponentFixture<PublicationRubricListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicationRubricListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicationRubricListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
