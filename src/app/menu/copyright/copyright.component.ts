import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-copyright',
  templateUrl: './copyright.component.html',
  styleUrls: ['./copyright.component.less']
})
export class CopyrightComponent implements OnInit {

  public current_date: Date;

  constructor() { }

  ngOnInit() {
    this.current_date = new Date();
  }

}
