import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {RubricMenuComponent} from '../rubric/menu/rubric-menu.component';
import {PublicationModule} from '../publication/publication.module';
import {RubricModule} from '../rubric/rubric.module';
import {RouterModule} from '@angular/router';
import {SubscribeModule} from '../subscribe/subscribe.module';
import {SearchModule} from '../search/search.module';
import { AdultOnlyComponent } from './adult-only/adult-only.component';
import {PublicationPromoBlockModule} from '../publication-promo-block/publication-promo-block.module';
import { HomeLinkComponent } from './home-link/home-link.component';
import { CopyrightComponent } from './copyright/copyright.component';
import { FooterMenuComponent } from './footer-menu/footer-menu.component';
import {SiteMenuComponent} from './site-menu/site-menu.component';
import {StaticLinksModule} from '../static-links/static-links.module';
import { NewOldMenuComponent } from './new-old-menu/new-old-menu.component';
import { ScrollTopComponent } from './scroll-top/scroll-top.component';

@NgModule({
  imports: [
    CommonModule,
    RubricModule,
    RouterModule,
    SubscribeModule,
    SearchModule,
    PublicationPromoBlockModule,
    StaticLinksModule,
  ],
  declarations: [
    AdultOnlyComponent,
    HomeLinkComponent,
    CopyrightComponent,
    FooterMenuComponent,
    SiteMenuComponent,
    NewOldMenuComponent,
    ScrollTopComponent,
  ],
  exports: [
    SiteMenuComponent,
    AdultOnlyComponent,
    FooterMenuComponent,
    NewOldMenuComponent,
    ScrollTopComponent,
  ]
})
export class MenuModule {
}
