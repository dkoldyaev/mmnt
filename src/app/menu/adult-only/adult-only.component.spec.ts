import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdultOnlyComponent } from './adult-only.component';

describe('AdultOnlyComponent', () => {
  let component: AdultOnlyComponent;
  let fixture: ComponentFixture<AdultOnlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdultOnlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdultOnlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
