import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewOldMenuComponent } from './new-old-menu.component';

describe('NewOldMenuComponent', () => {
  let component: NewOldMenuComponent;
  let fixture: ComponentFixture<NewOldMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewOldMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewOldMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
