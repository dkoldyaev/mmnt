import { Component, OnInit } from '@angular/core';
import {SiteMenuComponent} from '../site-menu/site-menu.component';

@Component({
  selector: 'app-new-old-menu',
  templateUrl: './new-old-menu.component.html',
  styleUrls: ['./new-old-menu.component.less']
})
export class NewOldMenuComponent extends SiteMenuComponent implements OnInit {

}
