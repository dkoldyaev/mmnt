import {Component, HostListener, OnInit} from '@angular/core';
import {IsBrowserOrServerService} from '../../--services/is-browser-or-server.service';

@Component({
  selector: 'app-scroll-top',
  templateUrl: './scroll-top.component.html',
  styleUrls: ['./scroll-top.component.less']
})
export class ScrollTopComponent implements OnInit {

  show_after_percent: number = 50;
  scroll_speed: number = 300;
  button_show: boolean = false;

  constructor(
    public is_browser_or_server_service: IsBrowserOrServerService,
  ) { }

  ngOnInit() {
    this.tryScrollSize();
  }

  @HostListener('window:scroll', [])
  tryScrollSize() {
    if (this.is_browser_or_server_service.is_server) {
      this.button_show = false;
    } else {
      let screen_height = window.innerHeight;
      let current_scroll = window.scrollY;
      this.button_show = current_scroll >= screen_height * this.show_after_percent / 100;
    }
  }

  scrollToTop() {
    if (!this.button_show) {
      return;
    }
    if (this.is_browser_or_server_service.is_browser) {
      window.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
    }
  }

}
