import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-site-menu',
  templateUrl: './site-menu.component.html',
  styleUrls: ['./site-menu.component.less'],
  providers: []
})
export class SiteMenuComponent implements OnInit {

  @Input() public collapsed: boolean = true;
  @Input() public allow_toggle: boolean = true;
  @Input() public fixed: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  public toggleMenu() {
    if (!this.allow_toggle) {
      return;
    }
    this.collapsed = !this.collapsed;
  }

  public closeMenu() {
    this.collapsed = true;
  }

  public openMenu() {
    this.collapsed = false;
  }

}
