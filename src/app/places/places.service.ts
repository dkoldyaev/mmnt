import { Injectable } from '@angular/core';
import {DateListService} from '../--abstract/--date-list-service';
import {IPlace} from './place';

@Injectable()
export class PlacesService extends DateListService<IPlace> {

  public date_query_field: string = 'first_letter';

  public get_service_name() {
    return 'place';
  }

}
