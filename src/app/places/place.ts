import {IMage} from '../simple-elements/image/image';
import {IPhone, Phone} from '../--classes/phone';
import {Link} from '../--classes/link';
import moment = require('moment');
import {IMetaObject, MetaObject} from '../--classes/meta-object';

export interface IPlace extends IMetaObject {

  title: string;
  announce_image: IMage;
  description: string;
  address: string;
  phone: (IPhone|string)[];
  link: Link|string;
  special_conditions: Map<string, string>;

}

export class Place extends MetaObject implements IPlace {

  title: string;
  announce_image: IMage;
  description: string;
  address: string;
  phone: (IPhone|string)[];
  link: Link|string;
  special_conditions: Map<string, string>;

  public constructor(data: IPlace) {

    super(data);

    for (let key in data) {

      let value: any;

      switch (key) {

        case 'phone' :
          let phones: Phone[] = [];
          for (let phone_i in data[key]) {
            phones.push(new Phone(data[key][phone_i]));
          }
          value = phones;
          break;

        case 'link' :
          value = new Link(data[key]);
          break;

        default :
          value = data[key];

      }

      this[key] = value;

    }
  }

}
