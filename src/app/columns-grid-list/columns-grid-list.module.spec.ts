import { ColumnsGridListModule } from './columns-grid-list.module';

describe('ColumnsGridListModule', () => {
  let columnsGridListModule: ColumnsGridListModule;

  beforeEach(() => {
    columnsGridListModule = new ColumnsGridListModule();
  });

  it('should create an instance', () => {
    expect(columnsGridListModule).toBeTruthy();
  });
});
