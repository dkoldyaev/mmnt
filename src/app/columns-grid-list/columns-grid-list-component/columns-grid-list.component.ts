import {Component, ContentChild, HostListener, Input, OnInit, TemplateRef} from '@angular/core';
import {IsBrowserOrServerService} from '../../--services/is-browser-or-server.service';

@Component({
  selector: 'app-columns-grid-list',
  templateUrl: './columns-grid-list.component.html',
  styleUrls: ['./columns-grid-list.component.less']
})
export class ColumnsGridListComponent implements OnInit {

  @Input()
  columns_count: number = 4;

  @Input()
  max_columns_count: number = 4;

  @Input()
  object_list: any[] = [];

  @Input()
  infinityScrollAction: Function = null;

  @Input()
  columns: number[] = [0, 1, 2, 3];

  @Input()
  column_width: number = 240;

  @Input()
  column_margin: number = 20;

  @Input()
  banner_position: number = 2;

  @ContentChild(TemplateRef)
  template: TemplateRef<any>;

  constructor(
    public is_browser_or_server_service: IsBrowserOrServerService
  ) {}

  @HostListener('window:resize', [])
  onResize() {
    let new_columns_count = this.is_browser_or_server_service.is_server
      ? this.max_columns_count
      : this.calcColumnsCount(
        window.innerWidth,
        this.column_width,
        this.column_margin);
    if (new_columns_count > this.max_columns_count) {
      new_columns_count = this.max_columns_count;
    }
    if (new_columns_count !== this.columns_count) {
      this.columns_count = new_columns_count;
      this.columns = this.getColumnsRange(this.columns_count);
    }
  }

  calcColumnsCount(container_width: number, column_width: number, column_margin: number): number {
    let result = Math.floor(
      (container_width - column_margin) / (column_width + column_margin)
    );
    if (result > 4) {
      result = 4;
    }
    return result;
  }

  protected getColumnsRange(columns_count) {
    return Array.from(Array(columns_count).keys());
  }

  ngOnInit() {
  }

}
