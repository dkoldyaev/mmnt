import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnsGridListComponent } from './columns-grid-list.component';

describe('ColumnsGridListComponent', () => {
  let component: ColumnsGridListComponent;
  let fixture: ComponentFixture<ColumnsGridListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColumnsGridListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumnsGridListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
