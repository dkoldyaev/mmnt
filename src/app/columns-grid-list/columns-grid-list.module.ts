import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ColumnsGridListComponent } from './columns-grid-list-component/columns-grid-list.component';
import {BannerModule} from '../banner/banner.module';

@NgModule({
  imports: [
    CommonModule,
    BannerModule,
  ],
  exports: [
    ColumnsGridListComponent,
  ],
  declarations: [
    ColumnsGridListComponent,
  ]
})
export class ColumnsGridListModule { }
