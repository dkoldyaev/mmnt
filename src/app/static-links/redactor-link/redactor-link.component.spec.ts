import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedactorLinkComponent } from './redactor-link.component';

describe('RedactorLinkComponent', () => {
  let component: RedactorLinkComponent;
  let fixture: ComponentFixture<RedactorLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedactorLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedactorLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
