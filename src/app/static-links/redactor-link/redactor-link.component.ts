import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-redactor-link',
  templateUrl: './redactor-link.component.html',
  styleUrls: ['./redactor-link.component.less']
})
export class RedactorLinkComponent implements OnInit {

  @Input()
  title: string = 'Редакция';

  constructor() { }

  ngOnInit() {
  }

}
