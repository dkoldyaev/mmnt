import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HotLinksComponent} from './hot-links/hot-links.component';
import {MainSectionLinksComponent} from './main-section-links/main-section-links.component';
import {StaticLinksComponent} from './static-links/static-links.component';
import {RouterModule} from '@angular/router';
import { RedactorLinkComponent } from './redactor-link/redactor-link.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
  ],
  declarations: [
    HotLinksComponent,
    MainSectionLinksComponent,
    StaticLinksComponent,
    RedactorLinkComponent,
  ],
  exports: [
    HotLinksComponent,
    MainSectionLinksComponent,
    StaticLinksComponent,
    RedactorLinkComponent,
  ],
})
export class StaticLinksModule { }
