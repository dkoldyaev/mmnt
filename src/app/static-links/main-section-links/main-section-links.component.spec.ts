import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainSectionLinksComponent } from './main-section-links.component';

describe('MainSectionLinksComponent', () => {
  let component: MainSectionLinksComponent;
  let fixture: ComponentFixture<MainSectionLinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainSectionLinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainSectionLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
