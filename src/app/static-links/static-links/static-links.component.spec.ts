import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaticLinksComponent } from './static-links.component';

describe('StaticLinksComponent', () => {
  let component: StaticLinksComponent;
  let fixture: ComponentFixture<StaticLinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaticLinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaticLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
