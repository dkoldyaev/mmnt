import { BrowserModule } from '@angular/platform-browser';
import {APP_ID, Inject, NgModule, PLATFORM_ID} from '@angular/core';

import { AppComponent } from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {MenuModule} from './menu/menu.module';
import {TagModule} from './tag/tag.module';
import {PublicationPromoBlockModule} from './publication-promo-block/publication-promo-block.module';
import {SubscribeModule} from './subscribe/subscribe.module';
import {BannerModule} from './banner/banner.module';
import {PublicationModule} from './publication/publication.module';
import {environment} from '../environments/environment';
import {HomeModule} from './home/home.module';
import {EventsDetailComponent} from './events/events-detail/events-detail.component';
import {EventsListComponent} from './events/events-list/events-list.component';
import {EventsModule} from './events/events.module';
import {HttpClientJsonpModule, HttpClientModule} from '@angular/common/http';
import {ShareButtonsModule} from '@ngx-share/buttons';
import {PublicationListComponent} from './publication-list/publication-list-component/publication-list.component';
import {
  PublicationDetailWithLoadingComponent
} from './publication/publication-detail-with-loading/publication-detail-with-loading.component';
import {MainItemListComponent} from './home/main-item-list/main-item-list.component';
import {PublicationRubricListComponent} from './publication-list/publication-rubric-list/publication-rubric-list.component';
import {PublicationDetailComponent} from './publication/publication-detail-component/publication-detail.component';
import {PublicationPreviewComponent} from './publication/publication-preview/publication-preview.component';
import {NotFoundComponent} from './not-found/not-found/not-found.component';
import {NotFoundModule} from './not-found/not-found.module';

import {SearchResultComponent} from './search/search-result/search-result.component';
import {CommonModule, isPlatformBrowser} from '@angular/common';
import {SafeHtmlPipe} from './--pipes/safe-html/safe-html.pipe';
import {SafeResourcePipe} from './--pipes/safe-resource/safe-resource.pipe';

export const appRoutes: Routes = [

  { path: '404', component: NotFoundComponent, pathMatch: 'full' },

  { path: 'search/:query', component: SearchResultComponent, pathMatch: 'full' },

  { path: 'events/:date_start/:date_end', component: EventsListComponent, pathMatch: 'full' },
  { path: 'events/:event_type_slug/:id', component: EventsDetailComponent, pathMatch: 'full' },
  { path: 'events/:event_type_slug', component: EventsListComponent, pathMatch: 'full' },
  { path: 'events', component: EventsListComponent, pathMatch: 'full' },

  { path: 'preview/:id', component: PublicationPreviewComponent, pathMatch: 'full' },
  { path: ':rubric_slug/:id', component: PublicationDetailWithLoadingComponent, pathMatch: 'full' },
  { path: ':rubric_slug', component: PublicationRubricListComponent, pathMatch: 'full' },

  { path: '', component: MainItemListComponent, pathMatch: 'full' },

  { path: '**', redirectTo: '/404' },

];
export const Routing = RouterModule.forRoot(
  appRoutes,
  { enableTracing: false/*environment.debug*/ }
);

@NgModule({
  declarations: [
    AppComponent,
    SafeHtmlPipe,
  ],
  imports: [
    CommonModule,

    HomeModule,
    PublicationModule,
    EventsModule,
    PublicationPromoBlockModule,
    MenuModule,
    TagModule,
    SubscribeModule,
    BannerModule,
    NotFoundModule,

    HttpClientModule,
    HttpClientJsonpModule,
    ShareButtonsModule.forRoot(),

    Routing,
  ],
  bootstrap: [
    AppComponent,
  ],
  providers: [],
  exports: [
    SafeHtmlPipe,
  ]
})
export class AppModule {

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string
  ) {
    const platform = isPlatformBrowser(platformId) ? 'in the browser' : 'on the server';
    // console.log(`Running ${platform} with appId=${appId}`);
  }

}
