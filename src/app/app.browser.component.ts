import {Component, OnInit} from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {IMetaObject, MetaObject} from './--classes/meta-object';
import {MetaService} from './--services/meta.service';
import {Metrika} from 'ng-yandex-metrika';
import {Location} from '@angular/common';
import {NavigationEnd, Router} from '@angular/router';
import {} from '@types/google.analytics';
import {ShadowService} from './--services/shadow.service';
import {element} from 'protractor';
import {IsBrowserOrServerService} from './--services/is-browser-or-server.service';
import {AppComponent} from './app.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  providers: [
    MetaService,
    ShadowService,
  ]
})
export class AppBrowserComponent extends AppComponent implements OnInit {

  changeMetaData(meta_object: MetaObject) {
    document.title = meta_object.toMetaTitle();
    super.changeMetaData(meta_object);
  }

  changeUrl(meta_data: MetaObject, force: boolean = false) {

    if (!meta_data) {
      return;
    }

    // Текущий юрл
    let new_path = this.location.path();
    let new_title = meta_data.toMetaTitle();

    if (new_path.indexOf('preview') >= 0) {
      return;
    }

    if (this.first_load_path && !this.old_path) {
      new_path = this.first_load_path;
      force = true;
      this.first_load_path = null;
    }

    if (this.old_path === new_path && !force) {
      return;
    }
    this.old_path = new_path;

    // Яндекс-метрика
    // console.log('Яндекс-метрика', new_path, {
    //   referer: this.old_path,
    //   title: new_title,
    // });
    this.metrika_service.hit(
      new_path,
      {
        referer: this.old_path,
        title: new_title,
      }
    );

    if (meta_data && 'id' in meta_data) {
      // Медиатор
      if (window['_mediator'].isActive()) {
        window['_mediator'].stop();
      }
      // console.log('Медиатор', new_path, {
      //   url: new_path,
      //   articleSelector: '#publication_' + meta_data['id'],
      // });
      window['_mediator'].start({
        url: `${window.location.protocol}//${window.location.host}${new_path}`,
        articleSelector: '#publication_' + meta_data['id'],
      });
    }

    // live-internet
    let liveinternet = new Image().src = '//counter.yadro.ru/hit?r'
      + encodeURI(document.referrer)
      + ((typeof(screen) === 'undefined') ? '' : ';s'
        + screen.width
        + '*'
        + screen.height
        + '*'
        + (screen.colorDepth ? screen.colorDepth : screen.pixelDepth))
      + ';u' + encodeURI(new_path)
      + ';h' + encodeURI(new_title.substring(0, 80))
      + ';'
      + Math.random();

    // Google-analytics
    // console.log('Google-analytics', {
    //   hitType:  'pageview',
    //   page:     new_path,
    //   title:    new_title,
    // });
    ga('send', {
      hitType:  'pageview',
      page:     new_path,
      title:    new_title,
    });
  }

  constructor(
    protected title_service: Title,
    protected meta_service: Meta,
    protected momenty_meta_service: MetaService,
    protected metrika_service: Metrika,
    protected location: Location,
    protected router: Router,
    protected shadow_service: ShadowService,
    public is_browser_or_server_service: IsBrowserOrServerService,
  ) {
    super(
      title_service,
      meta_service,
      momenty_meta_service,
      location,
      router,
      shadow_service,
      is_browser_or_server_service
    );
  }

  ngOnInit() {
    super.ngOnInit();
  }
}
