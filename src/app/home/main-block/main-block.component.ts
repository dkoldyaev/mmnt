import {Component, Input, OnInit} from '@angular/core';
import {MainBlock} from '../main_block';

@Component({
  selector: 'app-main-block',
  templateUrl: './main-block.component.html',
  styleUrls: ['./main-block.component.less']
})
export class MainBlockComponent implements OnInit {

  @Input()
  public main_block: MainBlock;

  @Input()
  public number: number;

  @Input()
  public is_big: boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
