import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PublicationModule} from '../publication/publication.module';
import {SubscribeModule} from '../subscribe/subscribe.module';
import {MainBlocksService} from './main-blocks.service';
import {BannerModule} from '../banner/banner.module';
import {PublicationPromoBlockModule} from '../publication-promo-block/publication-promo-block.module';
import {RouterModule, Routes} from '@angular/router';
import { MainBlockComponent } from './main-block/main-block.component';
import {MenuModule} from '../menu/menu.module';
import {SlickModule} from 'ngx-slick';
import {SearchModule} from '../search/search.module';
import { MainItemListComponent } from './main-item-list/main-item-list.component';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import { FilterByTypePipe } from './main-item-list/filter-by-type.pipe';
import {PublicationAnnounceModule} from '../publication-announce/publication-announce.module';

@NgModule({
  imports: [
    CommonModule,
    PublicationModule,
    SubscribeModule,
    BannerModule,
    PublicationPromoBlockModule,
    RouterModule,
    MenuModule,
    SlickModule,
    SearchModule,
    InfiniteScrollModule,
    PublicationAnnounceModule,
  ],
  declarations: [
    MainBlockComponent,
    MainItemListComponent,
    FilterByTypePipe,
  ],
  providers: [
    MainBlocksService
  ]
})
export class HomeModule { }
