import { Injectable } from '@angular/core';
import {DateListService} from '../--abstract/--date-list-service';
import {MainItem} from './main-item';

@Injectable()
export class MainItemService extends DateListService<MainItem> {

  public date_query_field = 'date';

  public get_service_name() {
    return 'main_item';
  }

  public list_url() {
    return super.list_url();
  }

}
