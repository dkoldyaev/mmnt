import {IPublication, Publication} from '../publication/publication';
import {IMage} from '../simple-elements/image/image';

export interface MainBlockInterface {

  id: number;

  publication_1: IPublication;
  image_1: IMage;
  image_type_1: string;
  subject_1: string;
  title_1: string;

  publication_2: IPublication;
  image_2: IMage;
  image_type_2: string;
  subject_2: string;
  title_2: string;

  resource_url: string;
}

export class MainBlock implements MainBlockInterface {

  id: number;

  publication_1: Publication;
  image_1: IMage;
  image_type_1: string;
  subject_1: string;
  title_1: string;

  publication_2: Publication;
  image_2: IMage;
  image_type_2: string;
  subject_2: string;
  title_2: string;

  resource_url: string;

  constructor(obj: MainBlockInterface) {
    for (let property_key in obj) {
      this[property_key] = obj[property_key];
    }
  }

  public isDouble() {
    return this.publication_2;
  }

  public isSingle() {
    return !this.isDouble();
  }

}
