import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DateListComponent} from '../../--abstract/--date-list-component';
import {MainItem} from '../main-item';
import {MainItemService} from '../main-item.service';
import {MainBlock} from '../main_block';
import {DatePipe} from '@angular/common';
import * as moment from 'moment';
import {IMetaObject, MetaObject} from '../../--classes/meta-object';
import {MetaService} from '../../--services/meta.service';
import {environment} from '../../../environments/environment';
import {ActivatedRoute} from '@angular/router';
import {Publication} from '../../publication/publication';
import {IsBrowserOrServerService} from '../../--services/is-browser-or-server.service';

@Component({
  selector: 'app-main-item-list',
  templateUrl: './main-item-list.component.html',
  styleUrls: [
    '../../publication-list/publication-list-component/publication-list.component.less',
    './main-item-list.component.less'
  ],
  providers: [
    MainItemService,
  ]
})
export class MainItemListComponent extends DateListComponent<MainItem, MainItemService> implements OnInit {

  public date_query_field = 'date';
  private already_show_small_block_ids: number[] = [];

  @Output()
  public change_meta_data: EventEmitter<IMetaObject> = new EventEmitter();

  constructor(
    protected service: MainItemService,
    protected meta_service: MetaService,
    protected route: ActivatedRoute,
    public is_browser_or_server_service: IsBrowserOrServerService,
  ) {
    super(service, is_browser_or_server_service, MainItem);
  }

  ngOnInit() {
    if (this.is_browser_or_server_service.is_browser) {
      window.scrollTo(0, 0);
    }
    const new_meta_data: MetaObject = new MetaObject({});
    this.meta_service.onMetaChanged.emit(new_meta_data);
    this.change_meta_data.emit(new_meta_data);
    this.getList();
  }

  newPageLoaded() {
    super.newPageLoaded();

    let left_col_count = this
      .object_list.filter(item => {
        return item.position === 'LEFT';
      }).length;
    let right_col_count = this
      .object_list.filter(item => {
        return item.position === 'RIGHT';
      }).length;
    let center_col_count = this
      .object_list.filter(item => {
        return item.position === 'CENTER';
      }).length;

    if (left_col_count < 5 || right_col_count < 5 || center_col_count < 5) {
      this.nextPage();
    }
  }

  onScrollDown() {
    this.nextPage();
  }

  onScrollUp() {
  }

  publicationIndex(publciation: Publication): number {
    for (let i in this.object_list) {
      if (publciation.id === this.object_list[i].id) {
        return parseInt(i, 10);
      }
    }
    return null;
  }

}
