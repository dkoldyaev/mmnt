import { Pipe, PipeTransform } from '@angular/core';
import {MainItem} from '../main-item';

@Pipe({
  name: 'filterByType'
})
export class FilterByTypePipe implements PipeTransform {

  transform(
    items: MainItem[],
    position_type: ('CENTER'|'RIGHT'|'LEFT')[],
    start_limit: number = null,
    page_size: number = null
  ): MainItem[] {
    let filtered_items = items.filter((item: MainItem) => {
      return position_type.indexOf(item.position) > -1;
    });
    if (start_limit !== null && page_size !== null) {
      filtered_items = filtered_items.slice(start_limit * page_size, start_limit * page_size + page_size);
    }
    return filtered_items;
  }

}
