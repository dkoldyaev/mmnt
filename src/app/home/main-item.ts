import {IPublication, Publication} from '../publication/publication';
import {IMage} from '../simple-elements/image/image';

export interface IMainItem {

  id: number;

  publication: IPublication;
  image: IMage;
  title: string;
  subject: string;

  position: 'LEFT'|'RIGHT'|'CENTER';
  // priority: number;
  // date: Date|string;

}

export class MainItem implements IMainItem {

  id: number;

  publication: Publication;
  image: IMage;
  title: string;
  subject: string;

  position: 'LEFT'|'RIGHT'|'CENTER';
  // priority: number;
  // date: Date|string;

  constructor(obj: IMainItem) {
    for (let property_key in obj) {
      if (property_key === 'publication') {
        obj[property_key] = new Publication(obj[property_key]);
      }
      this[property_key] = obj[property_key];
    }
  }

}
