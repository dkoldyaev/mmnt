import { TestBed, inject } from '@angular/core/testing';

import { MainItemService } from './main-item.service';

describe('MainItemService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MainItemService]
    });
  });

  it('should be created', inject([MainItemService], (service: MainItemService) => {
    expect(service).toBeTruthy();
  }));
});
