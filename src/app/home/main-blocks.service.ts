import {Injectable} from '@angular/core';
import {DateListService} from '../--abstract/--date-list-service';
import {MainBlock} from './main_block';

@Injectable()
export class MainBlocksService extends DateListService<MainBlock> {

  public get_service_name() {
    return 'main_blocks';
  }

}
