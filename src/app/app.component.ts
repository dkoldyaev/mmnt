import {Component, OnInit} from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {IMetaObject, MetaObject} from './--classes/meta-object';
import {MetaService} from './--services/meta.service';
import {Location} from '@angular/common';
import {NavigationEnd, Router} from '@angular/router';
import {} from '@types/google.analytics';
import {ShadowService} from './--services/shadow.service';
import {element} from 'protractor';
import {IsBrowserOrServerService} from './--services/is-browser-or-server.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  providers: [
    MetaService,
    ShadowService,
  ]
})
export class AppComponent implements OnInit {

  title = 'app';

  top_banner_show = false;
  show_shadow = false;

  protected old_path: string = null;
  protected first_load_path: string = null;

  bannerSwitch(show_banner: boolean) {
    this.top_banner_show = show_banner;
  }

  changeMetaData(meta_object: MetaObject) {
    this.title_service.setTitle(meta_object.toMetaTitle());
    if (this.is_browser_or_server_service.is_browser) {
      document.title = meta_object.toMetaTitle();
    }
    let definitions = meta_object.toMetaDefinition();
    for (let meta_i in meta_object.toMetaDefinition()) {
      this.meta_service.removeTag(meta_i);
      if (definitions[meta_i]) {
        for (let i in definitions[meta_i]) {
          this.meta_service.updateTag(definitions[meta_i][i]);
        }
      }
    }
    this.changeUrl(meta_object, false);
  }

  changeUrl(meta_data: MetaObject, force: boolean = false) {}

  showShadow() {
    this.setShadowState(true);
    this.shadow_service.onShadowShow.emit(this.show_shadow);
  }

  hideShadow() {
    this.setShadowState(false);
    this.shadow_service.onShadowShow.emit(this.show_shadow);
  }

  toggleShadow() {
    this.setShadowState(!this.show_shadow);
    this.shadow_service.onShadowShow.emit(this.show_shadow);
  }

  setShadowState(new_state: boolean) {
    this.show_shadow = new_state;
  }

  constructor(
    protected title_service: Title,
    protected meta_service: Meta,
    protected momenty_meta_service: MetaService,
    protected location: Location,
    protected router: Router,
    protected shadow_service: ShadowService,
    public is_browser_or_server_service: IsBrowserOrServerService,
  ) {
  }

  ngOnInit() {
    if (!this.is_browser_or_server_service.is_server) {
      this.first_load_path = window.location.pathname
        + (window.location.search ? '?' + window.location.search : '')
        + (window.location.hash ? window.location.hash : '');
    }
    this.momenty_meta_service.onMetaChanged.subscribe((meta_data: MetaObject) => {
      this.changeMetaData(meta_data);
    });
    this.shadow_service.onShadowShow.subscribe((show_shadow: boolean) => {
      this.setShadowState(show_shadow);
    });
  }
}
