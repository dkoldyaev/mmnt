import {Component, OnInit} from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {IMetaObject, MetaObject} from './--classes/meta-object';
import {MetaService} from './--services/meta.service';
import {Location} from '@angular/common';
import {NavigationEnd, Router} from '@angular/router';
import {} from '@types/google.analytics';
import {ShadowService} from './--services/shadow.service';
import {element} from 'protractor';
import {IsBrowserOrServerService} from './--services/is-browser-or-server.service';
import {AppComponent} from './app.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  providers: [
    MetaService,
    ShadowService,
  ]
})
export class AppServerComponent extends AppComponent implements OnInit {

  changeMetaData(meta_object: MetaObject) {
    super.changeMetaData(meta_object);
  }

  changeUrl(meta_data: MetaObject, force: boolean = false) {}

  constructor(
    protected title_service: Title,
    protected meta_service: Meta,
    protected momenty_meta_service: MetaService,
    protected location: Location,
    protected router: Router,
    protected shadow_service: ShadowService,
    public is_browser_or_server_service: IsBrowserOrServerService,
  ) {
    super(
      title_service,
      meta_service,
      momenty_meta_service,
      location,
      router,
      shadow_service,
      is_browser_or_server_service
    );
  }

  ngOnInit() {
    super.ngOnInit();
  }
}
