export interface IAuthor {

  first_name: string;
  last_name: string;

}

export class Author implements IAuthor {

  first_name: string;
  last_name: string;

  constructor(data: IAuthor) {
    this.first_name = data.first_name;
    this.last_name = data.last_name;
  }

  toString(): string {
    return (this.first_name + ' ' + this.last_name).trim();
  }

}
