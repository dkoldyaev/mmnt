import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicationAuthorComponent } from './publication-author/publication-author.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    PublicationAuthorComponent,
  ],
  declarations: [
    PublicationAuthorComponent,
  ]
})
export class AuthorModule { }
