import {Component, Input, OnInit} from '@angular/core';
import {Author} from '../author';

@Component({
  selector: 'app-publication-author',
  templateUrl: './publication-author.component.html',
  styleUrls: ['./publication-author.component.less']
})
export class PublicationAuthorComponent implements OnInit {

  @Input()
  authors: Author[] = [];

  constructor() { }

  ngOnInit() {
  }

}
