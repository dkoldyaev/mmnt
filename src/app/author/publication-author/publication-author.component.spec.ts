import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicationAuthorComponent } from './publication-author.component';

describe('PublicationAuthorComponent', () => {
  let component: PublicationAuthorComponent;
  let fixture: ComponentFixture<PublicationAuthorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicationAuthorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicationAuthorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
