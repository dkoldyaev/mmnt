import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShareSocialButtonsComponent } from './share-social-buttons/share-social-buttons.component';
import {ShareButtonsComponent, ShareButtonsModule} from '@ngx-share/buttons';
import {HttpClientJsonpModule, HttpClientModule} from '@angular/common/http';
import {ShareButtonComponent} from '@ngx-share/button';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    ShareButtonsModule,
    RouterModule,
  ],
  exports: [
    ShareSocialButtonsComponent,
  ],
  declarations: [
    ShareSocialButtonsComponent,
  ]
})
export class ShareModule { }
