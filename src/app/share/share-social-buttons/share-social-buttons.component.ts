import {Component, Input, OnInit} from '@angular/core';
import {ShareButtons} from '@ngx-share/core';
import {IMage} from '../../simple-elements/image/image';

@Component({
  selector: 'app-share-social-buttons',
  templateUrl: './share-social-buttons.component.html',
  styleUrls: ['./share-social-buttons.component.less'],
})
export class ShareSocialButtonsComponent implements OnInit {

  @Input() public label: string;

  @Input() public share_title: string;
  @Input() public share_description: string;
  @Input() public share_url: string;
  @Input() public share_image: IMage;

  constructor(public share: ShareButtons) { }

  ngOnInit() {
  }

}
