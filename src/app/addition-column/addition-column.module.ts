import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RightColComponent } from './right-col/right-col.component';
import {BannerModule} from '../banner/banner.module';
import {PublicationListModule} from '../publication-list/publication-list.module';
import {StickyModule} from 'ng2-sticky-kit';

@NgModule({
  imports: [
    CommonModule,
    BannerModule,
    PublicationListModule,
    StickyModule,
  ],
  declarations: [
    RightColComponent,
  ],
  exports: [
    RightColComponent,
  ],
})
export class AdditionColumnModule { }
