import {Component, Input, OnInit} from '@angular/core';
import {Publication} from '../../publication/publication';

@Component({
  selector: 'app-right-col',
  templateUrl: './right-col.component.html',
  styleUrls: ['./right-col.component.less']
})
export class RightColComponent implements OnInit {

  @Input()
  related_publications_filter: any[] = [];

  @Input()
  other_publications_count: number = 0;

  @Input()
  show_baner: boolean = true;

  @Input()
  show_related_publications: boolean = true;

  @Input()
  current_publication: Publication = null;

  constructor() { }

  ngOnInit() {
  }

}
