import {Component, Input, OnInit} from '@angular/core';
import {EVent} from '../event';

@Component({
  selector: 'app-events-announce',
  templateUrl: './events-announce.component.html',
  styleUrls: ['./events-announce.component.less']
})
export class EventsAnnounceComponent implements OnInit {

  @Input()
  event: EVent;

  constructor() { }

  ngOnInit() {
  }

}
