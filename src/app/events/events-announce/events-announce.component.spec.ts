import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsAnnounceComponent } from './events-announce.component';

describe('EventsSmallDetailComponent', () => {
  let component: EventsAnnounceComponent;
  let fixture: ComponentFixture<EventsAnnounceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventsAnnounceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsAnnounceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
