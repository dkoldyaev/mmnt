import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {DateListComponent} from '../../--abstract/--date-list-component';
import {EventsService} from '../events.service';
import {IEVent, EVent} from '../event';
import {DatePipe} from '@angular/common';
import moment = require('moment');
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {IsBrowserOrServerService} from '../../--services/is-browser-or-server.service';

export class WeekFilterItem {
  constructor(public start: Date, public end: Date) {}
  public monthChanged(): boolean {
    return this.start.getMonth() !== this.end.getMonth() || this.start.getFullYear() !== this.end.getFullYear();
  }
}

@Component({
  selector: 'app-events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.less'],
  providers: [EventsService]
})
export class EventsListComponent extends DateListComponent<EVent, EventsService> implements OnInit, OnDestroy {

  weeks: WeekFilterItem[] = [];

  filter_date_start: Date = null;
  filter_date_end: Date = null;
  today_date: Date = moment().toDate();
  tomorrow_date: Date = moment().add(1, 'day').toDate();

  date_query_field = 'date_start';

  events_list: {date: Date, events: EVent[]}[] = [];

  @Input()
  columns_count: number = 4;

  @Input()
  columns: number[] = [0, 1, 2, 3];

  private subs: Subscription;

  constructor(
    protected service: EventsService,
    protected route: ActivatedRoute,
    protected router: Router,
    public is_browser_or_server_service: IsBrowserOrServerService,
  ) {
    super(service, is_browser_or_server_service, EVent);
  }

  ngOnInit() {
    // Выставляем фильтры по неделям
    this.getWeeks();
    this.subs = this.route.params.subscribe(params => {
      if (!('date_start' in params && 'date_end' in params)) {
        this.changeDates(this.weeks[0].start, this.weeks[0].end);
      } else {
        this.filter_date_start = moment(params['date_start'], this.service.date_format).toDate();
        this.filter_date_end = moment(params['date_end'], this.service.date_format).toDate();
      }
    });
    // Фильтр на начало первой недели
    this.filters[this.date_query_field] = moment(this.filter_date_start).format(this.service.date_format);
    this.getList();
  }

  protected getWeeks(weeks_count: number = 4) {
    moment.locale('ru');
    let current_date = moment();

    for (let i = 0; i < weeks_count; i++) {
      this.weeks.push(new WeekFilterItem(
        i === 0 ? new Date() : current_date.startOf('week').toDate(),
        current_date.endOf('week').toDate()
      ));
      current_date = current_date.endOf('week').add(1, 'days');
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  public getCurrentWeek(): WeekFilterItem {
    let current_date = moment();
    for (let week_i in this.weeks) {
      if (this.weeks[week_i].start.getDay() === this.filter_date_start.getDay()
        && this.weeks[week_i].start.getMonth() === this.filter_date_start.getMonth()
        && this.weeks[week_i].start.getFullYear() === this.filter_date_start.getFullYear()
        && this.weeks[week_i].end.getDay() === this.filter_date_end.getDay()
        && this.weeks[week_i].end.getMonth() === this.filter_date_end.getMonth()
        && this.weeks[week_i].end.getFullYear() === this.filter_date_end.getFullYear()) {
        return this.weeks[week_i];
      }
    }
    return null;
  }

  public getNextWeek(): WeekFilterItem {
    if (!this.getCurrentWeek()) {
      return null;
    }
    let return_next = false;
    let counter = 0;
    for (let week_i in this.weeks) {
      if (return_next) {
        return this.weeks[week_i];
      }
      if (this.isCurrentWeek(this.weeks[week_i]) && counter < this.weeks.length - 1) {
        return_next = true;
      }
      counter++;
    }
    return null;
  }

  public isCurrentWeek(week: WeekFilterItem) {
    return this.filter_date_start.getFullYear() === week.start.getFullYear()
      && this.filter_date_start.getMonth() === week.start.getMonth()
      && this.filter_date_start.getDate() === week.start.getDate()
      && this.filter_date_end.getFullYear() === week.end.getFullYear()
      && this.filter_date_end.getMonth() === week.end.getMonth()
      && this.filter_date_end.getDate() === week.end.getDate();
  }

  public pageIsFull() {
    let result: boolean;
    if (this.queue.length === 0) {
      result = !this.can_load_next;
    } else {
      result = this.queue[this.queue.length - 1].date_start > this.filter_date_end || !this.can_load_next;
    }

    return result;
  }

  public queueSlice() {
    return this.queue;
  }

  public changeDates(date_start: Date, date_end: Date) {
    this.resetList();
    this.router.navigate([
      '/events',
      moment(date_start).format(this.service.date_format),
      moment(date_end).format(this.service.date_format),
    ]);
    this.filter_date_start = date_start;
    this.filter_date_end = date_end;
    this.filters[this.date_query_field] = moment(this.filter_date_start).format(this.service.date_format);
    this.getList();
  }

  public changeWeek(new_week: WeekFilterItem = null) {
    this.filter_date_start = new_week.start;
    this.filter_date_end = new_week.end;
    this.changeDates(new_week.start, new_week.end);
  }

  public resetWeek(date_start: Date, date_end: Date) {
    this.changeWeek(null);
    this.changeDates(date_start, date_end);
  }

  onResize() {
    let new_columns_count = this.is_browser_or_server_service.is_server
      ? 4
      : this.calcColumnsCount(window.innerWidth, 260, 30);
    if (new_columns_count !== this.columns_count) {
      this.columns_count = new_columns_count;
    }
  }

  newPageLoaded() {
    this.renderEvents();
  }

  public renderEvents() {
    for (let event_i in this.object_list) {
      let inserted = false;
      let event_date = this.object_list[event_i].date_start;
      for (let date_i in this.events_list) {
        let section_date: Date = this.events_list[date_i].date;
        if (event_date.getDay() === section_date.getDay()
          && event_date.getMonth() === section_date.getMonth()
          && event_date.getFullYear() === section_date.getFullYear()) {
          inserted = true;
          this.events_list[date_i].events.push(this.object_list[event_i]);
          break;
        }
      }
      if (!inserted) {
        this.events_list.push({
          date: event_date,
          events: [this.object_list[event_i]]
        });
      }
    }
  }

  calcColumnsCount(container_width: number, column_width: number, column_margin: number): number {
    let result = Math.floor(
      (container_width - column_margin) / (column_width + column_margin)
    );
    if (result > 4) {
      result = 4;
    }
    return result;
  }

  resetList() {
    super.resetList();
    this.events_list = [];
  }

  allowSwitchToNextWeek(): boolean {
    let i_num = 0;
    for (let week_i in this.weeks) {
      if (this.isCurrentWeek(this.weeks[week_i]) && i_num < this.weeks.length - 1) {
        return true;
      }
      i_num++;
    }
    return false;
  }

  switchToNextWeek() {
    let i_num = 0;
    let current_week = this.getCurrentWeek();
    if (current_week) {
      for (let week_i in this.weeks) {
        if (this.weeks[week_i].start === current_week.start
          && this.weeks[week_i].end === current_week.end
          && i_num < this.weeks.length - 1) {
          this.changeWeek(this.weeks[i_num + 1]);
          i_num++;
        }
      }
    }
  }

  public formatStartDate(week: WeekFilterItem): string {
    return moment(week.start).format(this.service.date_format);
  }
  public formatEndDate(week: WeekFilterItem): string {
    return moment(week.end).format(this.service.date_format);
  }

}
