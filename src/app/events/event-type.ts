export interface IEventType {
  id: number;
  slug: string;
  title: string;
  resource_uri: string;
}

export class EventType implements IEventType {

  id: number;
  slug: string;
  title: string;
  resource_uri: string;

  constructor(data: IEventType) {
    for (let key in data) {
      this[key] = data[key];
    }
  }

}
