import {IIMage} from '../simple-elements/image/image';
import {IPhone, Phone} from '../--classes/phone';
import {ILink, Link} from '../--classes/link';
import moment = require('moment');
import {IPlace, Place} from '../places/place';
import {EventType, IEventType} from './event-type';
import {IMetaObject, MetaObject} from '../--classes/meta-object';
import {DynamicInterface} from '../dynamic-content-module/dynamic.interface';

export interface IEVent extends IMetaObject {

  id: number;
  title: string;
  description: any;
  type: IEventType;

  places: IPlace[];

  date_start: Date|string;
  date_end: Date|string;

  announce_image: IIMage;

  age_limit: number;
  price_min: number;
  price_max: number;

  address: string;
  phone: (IPhone|string)[];
  link: ILink|string;

  special_conditions: ({title: string, data: string}[]|Map<string, string>);

}

export class EVent extends MetaObject implements IEVent {

  id: number;
  title: string;
  description: any;
  type: EventType;

  places: Place[] = [];

  date_start: Date;
  date_end: Date;

  announce_image: IIMage;

  age_limit: number;
  price_min: number;
  price_max: number;

  address: string;
  phone: (Phone|string)[] = [];
  link: Link|string;

  special_conditions: ({title: string, data: string}[]|Map<string, string>);

  days_between(): number {
    if (!this.date_end || !this.date_start) {
      return 0;
    }
    return moment(this.date_end).diff(this.date_start, 'days');
  }

  public is_simple_description(): boolean {
    return !Array.isArray(this.description);
  }

  public constructor(data: IEVent) {

    super(data);

    for (let key in data) {

      let value: any;

      switch (key) {

        case 'phone' :
          let phones: Phone[] = [];
          for (let phone_i in data[key]) {
            if (data[key][phone_i]) {
              phones.push(new Phone(data[key][phone_i]));
            }
          }
          value = phones;
          break;

        case 'link' :
          if (data[key]) {
            value = new Link(data[key]);
          }
          break;

        case 'places' :
          let places: Place[] = [];
          for (let place_i in data[key]) {
            if (data[key][place_i]) {
              let new_place = new Place(data[key][place_i]);
              places.push(new_place);
            }
          }
          value = places;
          break;

        case 'date_start':
        case 'date_end':
          if (!data[key]) {
            value = null;
          } else {
            if (data[key] instanceof Date) {
              value = data[key];
            } else {
              value = moment(data[key], 'YYYY-MM-DD HH:mm:ss').toDate();
            }
          }
          break;

        case 'special_conditions':
          value = [];
          for (let condition_key in data['special_conditions']) {
            value.push({
              title: condition_key,
              data: data['special_conditions'][condition_key],
            });
          }
          break;

        case 'type':
          value = new EventType(data[key]);
          break;

        case 'description':
          if (typeof data['description'] === 'string') {
            value = data['description'];
          } else {
            let paragraps: string[] = [];
            for (let part_i in this.description) {
              if ('content' in this.description[part_i]
                && 'type' in this.description[part_i]
                && this.description[part_i].type === 'html-p') {
                paragraps.push('<p>' + this.description[part_i].content + '</p>');
              }
            }
            value = paragraps.join('');
          }
          break;

        default :
          value = data[key];

      }

      this[key] = value;

    }

  }

}
