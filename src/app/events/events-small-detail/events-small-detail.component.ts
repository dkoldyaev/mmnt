import {Component, Input, OnInit} from '@angular/core';
import {EventsAnnounceComponent} from '../events-announce/events-announce.component';

@Component({
  selector: 'app-events-small-detail',
  templateUrl: './events-small-detail.component.html',
  styleUrls: ['./events-small-detail.component.less']
})
export class EventsSmallDetailComponent extends EventsAnnounceComponent implements OnInit {

  ngOnInit() {
  }

}
