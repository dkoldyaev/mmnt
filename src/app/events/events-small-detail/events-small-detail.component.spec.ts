import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsSmallDetailComponent } from './events-small-detail.component';

describe('EventsSmallDetailComponent', () => {
  let component: EventsSmallDetailComponent;
  let fixture: ComponentFixture<EventsSmallDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventsSmallDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsSmallDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
