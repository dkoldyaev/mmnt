import { Injectable } from '@angular/core';
import {DateListService} from '../--abstract/--date-list-service';
import {EVent, IEVent} from './event';

@Injectable()
export class EventsService extends DateListService<EVent> {

  public date_query_field: string = 'date_start';

  public get_service_name() {
    return 'event';
  }

}
