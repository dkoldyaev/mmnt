import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import { EventsListComponent } from './events-list/events-list.component';
import {RouterModule, Routes} from '@angular/router';
import { EventsDetailComponent } from './events-detail/events-detail.component';
import {NgPipesModule} from 'ngx-pipes';
import {EventsAnnounceComponent} from './events-announce/events-announce.component';
import {EventsSmallDetailComponent} from './events-small-detail/events-small-detail.component';
import {SimpleElementsModule} from '../simple-elements/simple-elements.module';

const eventsRoutes: Routes = [
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(eventsRoutes),
    NgPipesModule,
    SimpleElementsModule,
  ],
  declarations: [
    EventsListComponent,
    EventsDetailComponent,
    EventsSmallDetailComponent,
    EventsAnnounceComponent,
  ],
  exports: [
    EventsSmallDetailComponent,
    EventsAnnounceComponent,
  ],
})
export class EventsModule { }
