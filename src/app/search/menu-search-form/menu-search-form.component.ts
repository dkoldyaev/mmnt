import { Component, OnInit } from '@angular/core';
import {FormControl, NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {SearchFormComponent} from '../search-form/search-form.component';

@Component({
  selector: 'app-menu-search-form',
  templateUrl: './menu-search-form.component.html',
  styleUrls: ['./menu-search-form.component.less']
})
export class MenuSearchFormComponent extends SearchFormComponent implements OnInit {

  collapsed: boolean = true;

  constructor(
    protected router: Router
  ) {
    super(router);
  }

  ngOnInit() {
    this.searchField = new FormControl();
  }

  toggleSearch($event) {
    $event.preventDefault();
    this.collapsed = !this.collapsed;
  }

}
