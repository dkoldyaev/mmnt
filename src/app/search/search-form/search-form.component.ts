import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.less']
})
export class SearchFormComponent implements OnInit {

  @Input()
  query: string = '';

  searchField: FormControl;

  constructor(
    protected router: Router
  ) { }

  ngOnInit() {
    this.searchField = new FormControl();
  }

  onSubmit($event) {
    $event.preventDefault();
    this.router.navigate(['/search', this.searchField.value]);
  }

}
