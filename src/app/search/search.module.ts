import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuSearchFormComponent } from './menu-search-form/menu-search-form.component';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SearchResultComponent } from './search-result/search-result.component';
import {SearchFormComponent} from './search-form/search-form.component';
import {PublicationAnnounceModule} from '../publication-announce/publication-announce.module';
import {ColumnsGridListModule} from '../columns-grid-list/columns-grid-list.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    PublicationAnnounceModule,
    ColumnsGridListModule,
  ],
  exports: [
    MenuSearchFormComponent
  ],
  declarations: [
    SearchFormComponent,
    MenuSearchFormComponent,
    SearchResultComponent,
  ]
})
export class SearchModule { }
