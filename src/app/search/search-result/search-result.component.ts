import {AfterViewInit, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {PublicationListComponent} from '../../publication-list/publication-list-component/publication-list.component';
import moment = require('moment');
import {PublicationService} from '../../publication/publication.service';
import {IsBrowserOrServerService} from '../../--services/is-browser-or-server.service';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.less'],
  providers: [
    PublicationService,
  ]
})
export class SearchResultComponent extends PublicationListComponent implements OnInit, OnDestroy, AfterViewInit {

  query: string = '';
  columns_count: number = 4;

  @Input()
  column_width: number = 240;

  @Input()
  column_margin: number = 20;

  @Input()
  columns: number[] = [];

  private route_subscribe: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    protected service: PublicationService,
    public is_browser_or_server_service: IsBrowserOrServerService,
  ) {
    super(service, is_browser_or_server_service);
  }

  ngOnInit() {

    this.columns = [];
    for (let i = 0; i < this.columns_count; i++) {
      this.columns[i] = i;
    }

    this.route_subscribe = this.route.params.subscribe((params) => {
      this.query = params['query'].trim();
      this.resetList();
      this.filters[this.date_query_field] = moment().format(this.service.date_format);
      this.filters['query'] = this.query;
      this.getList();
    });

  }

  ngOnDestroy() {
    this.route_subscribe.unsubscribe();
  }

  ngAfterViewInit() {
    this.onResize();
  }

  onResize() {
    let new_columns_count = this.is_browser_or_server_service.is_server
      ? 4
      : this.calcColumnsCount(window.innerWidth, this.column_width, this.column_margin);
    if (new_columns_count !== this.columns_count) {
      this.columns_count = new_columns_count;
    }
  }

  calcColumnsCount(container_width: number, column_width: number, column_margin: number): number {
    let result = Math.floor(
      (container_width - column_margin) / (column_width + column_margin)
    );
    if (result > 4) {
      result = 4;
    }
    return result;
  }

  onScrollDown() {
    this.nextPage();
  }



}
