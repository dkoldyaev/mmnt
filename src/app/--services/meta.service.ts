import {EventEmitter, Injectable} from '@angular/core';
import {IMetaObject, MetaObject} from '../--classes/meta-object';

@Injectable()
export class MetaService {

  onMetaChanged: EventEmitter<IMetaObject> = new EventEmitter<IMetaObject>();

}
