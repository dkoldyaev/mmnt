import { TestBed, inject } from '@angular/core/testing';

import { IsBrowserOrServerService } from './is-browser-or-server.service';

describe('IsBrowserOrServerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsBrowserOrServerService]
    });
  });

  it('should be created', inject([IsBrowserOrServerService], (service: IsBrowserOrServerService) => {
    expect(service).toBeTruthy();
  }));
});
