import {EventEmitter, Injectable} from '@angular/core';

@Injectable()
export class ShadowService {

  onShadowShow: EventEmitter<boolean> = new EventEmitter<boolean>();

}
