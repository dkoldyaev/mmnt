import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {isPlatformBrowser} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class IsBrowserOrServerService {

  public is_browser: boolean;
  public is_server: boolean;

  constructor(
    @Inject(PLATFORM_ID) protected platformId: Object
  ) {
    this.is_browser = isPlatformBrowser(platformId);
    this.is_server = !this.is_browser;
  }
}
