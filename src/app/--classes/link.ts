export interface ILink {
  value: string;
}

export class Link implements ILink {

  value: string;

  constructor(val: string|ILink) {
    if (typeof val === 'string') {
      this.value = val;
    } else {
      this.value = val.value;
    }
  }

  toString() {
    return this.value;
  }

}
