import {MetaDefinition} from '@angular/platform-browser';
import {IMage} from '../simple-elements/image/image';
import {Author} from '../author/author';
import {Rubric} from '../rubric/rubric';

export interface IMetaObject {

  meta_title?: string;
  meta_description?: string;
  meta_keywords?: string;
  meta_og_title?: string;
  meta_og_description?: string;
  meta_og_image?: IMage;

}

export class MetaObject implements IMetaObject {

  meta_title: string;
  meta_description: string;
  meta_keywords: string;
  meta_og_title: string;
  meta_og_description: string;
  meta_og_image: IMage;

  constructor(data: IMetaObject) {
    for (let property_key in data) {
      this[property_key] = data[property_key];
    }
  }

  toMetaTitle(): string {
    if (!this.meta_title) {
      return 'Моменты. Красивая жизнь интересных людей. Спецпроект URA.RU.';
    }
    return this.meta_title + ' - Моменты';
  }

  toMetaDefinition(): {[name: string]: MetaDefinition[]} {

    let og_image = 'https://momenty.org/site/media/default/i/cover.png';
    if (this.meta_og_image && ('url' in this.meta_og_image) && this.meta_og_image.url) {
      og_image = this.meta_og_image.url;
    }

    let meta_description = this.meta_description
      ? this.meta_description
      : 'Моменты. Красивая жизнь интересных людей. Читайте на страницах спецпроекта Ура.ру.';

    let result: {[name: string]: MetaDefinition[]} = {

      'name="description"': [{
        name: 'description',
        content: meta_description}],
      'name="keywords"': [{
        name: 'keywords',
        content: this.meta_keywords
          ? this.meta_keywords
          : 'Моменты Екатеринбург, Моменты URA RU, Моменты Ура ру, Моменты спецпроект URA RU, Моменты спецпроект Ура ру'
      }],

      'property="og:title"': [{
        property: 'og:title',
        content: this.meta_og_title || this.meta_title}],
      'property="og:description"': [{
        property: 'og:description',
        content: this.meta_og_description || meta_description}],
      'property="og:image"': [{
        property: 'og:image', content: og_image}],

      'name="mediator"': null,
      'name="mediator_theme"': null,
      'name="mediator_author"': null,
      'name="mediator_published_time"': null,

    };

    return result;
  }

}
