export interface IPhone {
  value: string;
}

export class Phone implements IPhone {

  value: string;

  constructor(val: string|IPhone) {
    if (typeof val === 'string') {
      this.value = val;
    } else {
      this.value = val.value;
    }
  }

  toString() {
    return this.value;
  }
}
