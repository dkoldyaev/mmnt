export interface IDataList<DateListObjectType> {

  objects: DateListObjectType[];

  meta: {
    next: string,
    previous: string,

    next_date?: string|Date,

    limit: number,
    offset: number,
    total_count: number
  };

}
