import {Observable} from 'rxjs';
import {IDataList} from './--date-list';
import {Injectable} from '@angular/core';
import * as moment from 'moment';
import {ApiListService} from './--api-list-service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {IsBrowserOrServerService} from '../--services/is-browser-or-server.service';

@Injectable()
export class DateListService<DateListObjectType> extends ApiListService {

  public date_format: string = 'YYYY-MM-DD';

  public date_query_field: string = 'publish_at';

  public filters: any = {};

  public setFilters(filters) {
    this.filters = filters;

    if (!(this.date_query_field in this.filters)) {
      if (typeof this.filters[this.date_query_field] !== 'string') {
        if (this.filters[this.date_query_field] instanceof Date) {
          this.filters[this.date_query_field] = moment(this.filters[this.date_query_field]).format(this.date_format);
        } else if (this.is_browser_or_server_service.is_server) {
          this.filters[this.date_query_field] = moment().format(this.date_format);
        }
      }
    }

    // Сортируем ключи по алфавиту
    this.filters = Object.entries(this.filters).sort().reduce((o, [k, v]) => (o[k] = v, o), {});
    this.applyFilter();
  }

  public applyFilter() {
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json');

    let params = {};
    for (let filter_name in this.filters) {
      params[filter_name] = this.filters[filter_name];
    }

    this.options = {
      headers: headers,
      params: params,
    };
  }

  public getList(filters: any = null): Observable<IDataList<DateListObjectType>> {
    if (filters) {
      this.setFilters(filters);
    }
    return super._getList();
  }

  public getDetail(publication_id_or_url: number|string): Observable<DateListObjectType> {

    if (typeof publication_id_or_url === 'number') {
      return this.http.get<DateListObjectType>(
        this.detail_url(publication_id_or_url)
      );
    }
    return super._getDetail(
      publication_id_or_url
    );
  }

  public constructor(
    protected http: HttpClient,
    protected router: Router,
    protected route: ActivatedRoute,
    public is_browser_or_server_service: IsBrowserOrServerService
  ) {
    super(http, router, route);
  }

}
