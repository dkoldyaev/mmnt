import {Input} from '@angular/core';
import {DateListService} from './--date-list-service';
import {IsBrowserOrServerService} from '../--services/is-browser-or-server.service';

interface DateListObjectTypeEntity<DateListObjectType> {
  new (any): DateListObjectType;
}

export class DateListComponent<DateListObjectType, DateListObjectService extends DateListService<DateListObjectType>> {

  @Input()
  public page_size: number = 10;

  @Input()
  public page_num: number = 1;

  @Input()
  public allow_load_next: boolean = true;

  @Input()
  public filters: any = {};

  // Список объектов
  public object_list: DateListObjectType[] = [];

  // Достингут конец списка
  public end: boolean = false;

  // Загружается сейчас
  public loading: boolean = false;

  // Если возможность загружать дальще
  protected can_load_next: boolean = true;

  // Очередь
  protected queue: DateListObjectType[] = [];

  // Формат даты
  private date_format: string = 'yyyy-MM-dd';

  // Поле для фильтрации по дате
  public date_query_field = 'publish_at';

  constructor(
    protected service: DateListObjectService,
    public is_browser_or_server_service: IsBrowserOrServerService,
    protected entity_constructor: DateListObjectTypeEntity<DateListObjectType>,
  ) {}

  protected filterElements(object: DateListObjectType): boolean {
    return true;
  }

  public pageIsFull(): boolean {
    return this.queue.length >= this.page_size * this.page_num || (!this.can_load_next && !this.end);
  }

  public getPageSlice(): DateListObjectType[] {
    return this.queue.slice(
      ((this.page_num - 1) * this.page_size),
      (this.page_num * this.page_size)
    );
  }

  public queueSlice(): DateListObjectType[] {
    let new_slice = this.getPageSlice();
    for (let new_slice_i in new_slice) {
      new_slice[new_slice_i]['page_num'] = this.page_num;
    }
    let result = this.object_list.concat(new_slice);
    return result;
  }

  public getList(): void {

    if (this.end) {
      return;
    }

    if (this.pageIsFull()) {

      this.loading = false;
      this.object_list = this.queueSlice();
      // Если в очереди не осталось элементов
      // и мы не можем больше загружать,
      // то ставим, что конец очереди
      this.end = this.queue.length <= this.object_list.length && !this.can_load_next;
      this.newPageLoaded();

    } else if (this.can_load_next) {

      this.loading = true;

      this.service.getList(this.filters)
        .subscribe((data) => {
          const new_data = data.objects;
          const new_meta = data.meta;
          let new_publication: DateListObjectType;
          for (let obj_i in new_data) {
            new_publication = new this.entity_constructor(new_data[obj_i]);
            if (this.filterElements(new_publication)) {
              this.queue.push(new_publication);
            }
          }
          if ('next_date' in new_meta && new_meta.next_date) {
            this.filters[this.date_query_field] = new_meta.next_date;
          } else {
            this.can_load_next = false;
          }
          this.getList();
        });

    }

  }

  public nextPage() {
    if (this.loading) {
      return;
    }
    if (!this.end) {
      this.page_num++;
      this.getList();
    }
  }

  public resetList() {
    this.page_num = 1;
    this.object_list = [];
    this.queue = [];
    this.can_load_next = true;
    this.end = false;
    this.filters = {};
  }

  public newPageLoaded(): void {
  }

}
