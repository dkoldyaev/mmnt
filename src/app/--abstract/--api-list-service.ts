import {Inject, Injectable, Optional} from '@angular/core';
import {environment} from '../../environments/environment';
import {ActivatedRoute, Router} from '@angular/router';

import {Observable} from 'rxjs';
import {IDataList} from './--date-list';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class ApiListService {

  protected options = {};
  private base_api_host: string = '/api/v1';

  public constructor(
    protected http: HttpClient,
    protected router: Router,
    protected route: ActivatedRoute
  ) {
    this.base_api_host = `${location.protocol}//${location.host}${this.base_api_host}`;
  }

  public get_service_name(): string {
    throw new Error('Переопределите эту функцию');
  }

  public related_list_url(): string {
    return this.base_api_host + '/' + this.get_service_name() + '/';
  }

  public list_url(): string {
    return environment.api_host + this.related_list_url();
  }

  public related_detail_url(id) {
    return this.related_list_url() + id + '/';
  }

  public detail_url(id): string {
    return environment.api_host + this.related_detail_url(id);
  }

  public _getList(): Observable<any> {
    return this.http.get(
      this.list_url(),
      this.options
    );
  }

  public _getDetail(id): Observable<any> {
    return this.http.get(
      this.detail_url(id)
    );
  }

}
