import {Component, Input, OnInit} from '@angular/core';
import {Publication} from '../../publication/publication';
import {isNull} from 'util';
import {IMage} from '../../simple-elements/image/image';

@Component({
  moduleId: module.id,
  selector: 'app-publication-announce',
  styleUrls: ['./publication-announce.less'],
  templateUrl: './publication-announce.html',
})
export class PublicationAnnounceComponent implements OnInit {

  @Input()
  public publication: Publication;

  @Input()
  public image: IMage;

  @Input()
  public image_type: string = 'square';

  @Input()
  public subject: string;

  @Input()
  public title: string;

  @Input()
  public show_categories: boolean = false;
  @Input()
  public show_date: boolean = null;
  @Input()
  public show_time: boolean = null;
  @Input()
  public show_datetime: boolean = false;

  ngOnInit() {
    if (!this.publication) {
      return;
    }
    if (!this.title) {
      this.title = this.publication.title;
    }
    if (!this.image) {
      this.image = this.publication.announce_image;
    }
    if (!this.subject) {
      this.subject = this.publication.subject || (this.publication.primary_rubric && this.publication.primary_rubric.title);
    }

    if (isNull(this.show_date) && isNull(this.show_time)) {
      this.show_date = this.show_datetime;
      this.show_time = this.show_datetime;
    }
  }

  public constructor() {
  }
}
