import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PublicationAnnounceComponent} from './publication-announce-component/publication-announce.component';
import {RouterModule} from '@angular/router';
import {SimpleElementsModule} from '../simple-elements/simple-elements.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SimpleElementsModule,
  ],
  exports: [
    PublicationAnnounceComponent,
  ],
  declarations: [
    PublicationAnnounceComponent,
  ]
})
export class PublicationAnnounceModule { }
