import {Component, Input, OnInit} from '@angular/core';
import {DateComponent} from '../date/date.component';
import moment = require('moment');
import {DateTimeComponent} from '../date-time/date-time.component';

@Component({
  selector: 'app-date-calendar',
  templateUrl: './date-calendar.component.html',
  styleUrls: ['./date-calendar.component.less']
})
export class DateCalendarComponent extends DateTimeComponent implements OnInit {

  @Input()
  use_yesterday: boolean = true;

  toCalendar() {
    return moment(this.date)
      .locale(this.locale)
      .calendar(
      null,
      {
        sameDay:  '[Сегодня]',
        nextDay:  '[Завтра]',
        nextWeek: 'dddd',
        lastDay:  (this.use_yesterday ? '[Вчера]' : this.date_format),
        lastWeek: this.date_format,
        sameElse: this.date_format,
      }
    );
  }

  ngOnInit() {
  }

}
