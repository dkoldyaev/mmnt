import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-date-time',
  templateUrl: './date-time.component.html',
  styleUrls: ['./date-time.component.less', ]
})
export class DateTimeComponent implements OnInit {

  @Input()
  date: Date = new Date();

  @Input()
  date_format: string = 'D MMMM YYYY';

  @Input()
  time_format: string = 'LT';

  @Input()
  locale: string = 'ru';

  @Input()
  show_date: boolean = true;

  @Input()
  show_time: boolean = false;

  format(): string {
    return this.date_format + ' ' + this.time_format;
  }

  constructor() { }

  ngOnInit() {
  }
}
