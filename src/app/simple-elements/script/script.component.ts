import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {IsBrowserOrServerService} from '../../--services/is-browser-or-server.service';

@Component({
  selector: 'app-script',
  templateUrl: './script.component.html',
  styleUrls: ['./script.component.less']
})
export class ScriptHackComponent implements AfterViewInit {

  @Input()
  src: string;

  @Input()
  type: string;

  @Input()
  inner_script: string;

  @ViewChild('script')
  script: ElementRef;

  constructor(
    public is_browser_or_server_service: IsBrowserOrServerService
  ) {}

  convertToScript() {
    if (this.is_browser_or_server_service.is_server) {
      return;
    }
    let element = this.script.nativeElement;
    let script = document.createElement('script');
    script.type = this.type ? this.type : 'text/javascript';
    if (this.src) {
      script.src = this.src;
    }
    if (this.inner_script) {
      script.innerHTML = this.inner_script;
    } else if (element.innerHTML) {
      script.innerHTML = element.innerHTML;
    }
    let parent = element.parentElement;
    parent.parentElement.replaceChild(script, parent);
  }

  ngAfterViewInit() {
    this.convertToScript();
  }
}
