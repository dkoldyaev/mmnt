import {Component, Input, OnInit} from '@angular/core';
import {IMage} from './image';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.less']
})
export class ImageComponent implements OnInit {

  @Input()
  image: IMage;

  @Input()
  show_caption: boolean = true;

  constructor() { }

  ngOnInit() {
  }

}
