export interface IIMage {

  url: string;
  title?: string;
  author?: string;
  width?: number;
  height?: number;

}

export class IMage implements IIMage {

  url: string;
  title: string;
  author: string;
  width: number;
  height: number;

  public link(): string {
    if (/^upload/i.test(this.url)) {
      return this.url.replace('upload/', '/media/');
    }
    return this.url;
  }

  constructor(obj: IIMage) {
    for (let prop_i in obj) {
      this[prop_i] = obj[prop_i];
    }
  }

}
