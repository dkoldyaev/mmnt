import {Component, Input, OnInit} from '@angular/core';
import {IMage} from '../image/image';

@Component({
  selector: 'app-image-link',
  templateUrl: './image-link.component.html',
  styleUrls: ['./image-link.component.less']
})
export class ImageLinkComponent implements OnInit {

  @Input()
  link_params: any[];

  @Input()
  image: IMage;

  constructor() { }

  ngOnInit() {
  }

}
