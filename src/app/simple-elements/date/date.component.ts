import {Component, Input, OnInit} from '@angular/core';
import * as moment from 'moment';
import {DateTimeComponent} from '../date-time/date-time.component';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.less']
})
export class DateComponent extends DateTimeComponent {
}
