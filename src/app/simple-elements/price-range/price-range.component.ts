import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-price-range',
  templateUrl: './price-range.component.html',
  styleUrls: ['./price-range.component.less']
})
export class PriceRangeComponent implements OnInit {

  @Input()
  price_min: number;

  @Input()
  price_max: number;

  @Input()
  currency: string = '₽';

  constructor() { }

  ngOnInit() {
  }

}
