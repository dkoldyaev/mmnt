import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageComponent } from './image/image.component';
import { ImageLinkComponent } from './image-link/image-link.component';
import {RouterModule} from '@angular/router';
import { ImageCaptionComponent } from './image-caption/image-caption.component';
import { DateComponent } from './date/date.component';
import {MomentModule} from 'ngx-moment';
import { DateTimeComponent } from './date-time/date-time.component';
import { DateCalendarComponent } from './date-calendar/date-calendar.component';
import { TimeComponent } from './time/time.component';
import {ScriptHackComponent} from './script/script.component';
import { PriceRangeComponent } from './price-range/price-range.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MomentModule,
  ],
  declarations: [
    ImageComponent,
    ImageLinkComponent,
    ImageCaptionComponent,
    DateComponent,
    DateTimeComponent,
    DateCalendarComponent,
    TimeComponent,
    ScriptHackComponent,
    PriceRangeComponent,
  ],
  exports: [
    ImageComponent,
    ImageLinkComponent,
    ImageCaptionComponent,
    DateComponent,
    DateTimeComponent,
    DateCalendarComponent,
    TimeComponent,
    ScriptHackComponent,
    PriceRangeComponent,
  ],
})
export class SimpleElementsModule { }
