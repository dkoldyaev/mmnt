import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-image-caption',
  templateUrl: './image-caption.component.html',
  styleUrls: ['./image-caption.component.less']
})
export class ImageCaptionComponent implements OnInit {

  @Input()
  title: string;

  @Input()
  author: string;

  constructor() { }

  ngOnInit() {
  }

}
