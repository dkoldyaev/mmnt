import { Component, OnInit } from '@angular/core';
import {DateTimeComponent} from '../date-time/date-time.component';

@Component({
  selector: 'app-time',
  templateUrl: './time.component.html',
  styleUrls: ['./time.component.less']
})
export class TimeComponent extends DateTimeComponent {
}
