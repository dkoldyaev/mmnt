import {NgModule} from '@angular/core';
import {AppModule, Routing} from './app.module';
import {ServerModule} from '@angular/platform-server';
import {ModuleMapLoaderModule} from '@nguniversal/module-map-ngfactory-loader';
import {AppServerComponent} from './app.server.component';
import {HomeModule} from './home/home.module';
import {PublicationModule} from './publication/publication.module';
import {EventsModule} from './events/events.module';
import {PublicationPromoBlockModule} from './publication-promo-block/publication-promo-block.module';
import {MenuModule} from './menu/menu.module';
import {TagModule} from './tag/tag.module';
import {SubscribeModule} from './subscribe/subscribe.module';
import {BannerModule} from './banner/banner.module';
import {NotFoundModule} from './not-found/not-found.module';
import {HttpClientJsonpModule, HttpClientModule} from '@angular/common/http';
import {ShareButtonsModule} from '@ngx-share/buttons';
import {BrowserModule} from '@angular/platform-browser';

@NgModule({
  declarations: [
    AppServerComponent
  ],
  imports: [
    AppModule,
    BrowserModule.withServerTransition({ appId: 'momenty' }),
    ServerModule,
    ModuleMapLoaderModule,

    HomeModule,
    PublicationModule,
    EventsModule,
    PublicationPromoBlockModule,
    MenuModule,
    TagModule,
    SubscribeModule,
    BannerModule,
    NotFoundModule,

    HttpClientModule,
    HttpClientJsonpModule,
    ShareButtonsModule.forRoot(),

    Routing,
  ],
  providers: [
  ],
  bootstrap: [
    AppServerComponent
  ]
})
export class AppServerModule {}
