import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from './not-found/not-found.component';
import {PublicationListModule} from '../publication-list/publication-list.module';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    PublicationListModule,
  ],
  declarations: [NotFoundComponent]
})
export class NotFoundModule { }
