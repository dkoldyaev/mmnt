import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscribePushComponent } from './subscribe-push.component';

describe('SubscribePushComponent', () => {
  let component: SubscribePushComponent;
  let fixture: ComponentFixture<SubscribePushComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubscribePushComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscribePushComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
