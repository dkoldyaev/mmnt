import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscribeEmailComponent } from './subscribe-email/subscribe-email.component';
import { SubscribeSocialButtonsComponent } from './subscribe-social-buttons/subscribe-social-buttons.component';
import {SubscribeService} from './subscribe.service';
import { SubscribeLineComponent } from './subscribe-line/subscribe-line.component';
import {RouterModule} from '@angular/router';
import { SubscribePushComponent } from './subscribe-push/subscribe-push.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
  ],
  declarations: [
    SubscribeSocialButtonsComponent,
    SubscribeEmailComponent,
    SubscribeLineComponent,
    SubscribePushComponent,
  ],
  providers: [
    SubscribeService
  ],
  exports: [
    SubscribeSocialButtonsComponent,
    SubscribeEmailComponent,
    SubscribeLineComponent,
    SubscribePushComponent,
  ]
})
export class SubscribeModule { }
