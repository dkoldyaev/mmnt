import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-subscribe-email',
  templateUrl: './subscribe-email.component.html',
  styleUrls: [
    './subscribe-email.component.less'
  ],
  providers: []
})
export class SubscribeEmailComponent implements OnInit {

  @Input()
  public title: string = 'Еженедельная рассылка';

  @Input()
  public form_id = Math.random().toString(36).substr(2, 5);

  constructor() { }

  ngOnInit() {
  }

}
