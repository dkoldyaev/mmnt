import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscribeLineComponent } from './subscribe-line.component';

describe('SubscribeLineComponent', () => {
  let component: SubscribeLineComponent;
  let fixture: ComponentFixture<SubscribeLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubscribeLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscribeLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
