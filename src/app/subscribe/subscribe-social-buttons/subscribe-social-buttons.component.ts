import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-subscribe-social-buttons',
  templateUrl: './subscribe-social-buttons.component.html',
  styleUrls: [
    './subscribe-social-buttons.component.less'
  ],
  providers: []
})
export class SubscribeSocialButtonsComponent implements OnInit {

  @Input()
  public title: string = 'Соцсети';

  constructor() { }

  ngOnInit() {
  }

}
