import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscribeSocialButtonsComponent } from './subscribe-social-buttons.component';

describe('SubscribeSocialButtonsComponent', () => {
  let component: SubscribeSocialButtonsComponent;
  let fixture: ComponentFixture<SubscribeSocialButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubscribeSocialButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscribeSocialButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
