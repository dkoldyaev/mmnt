import { Component, OnInit } from '@angular/core';
import {PublicationService} from '../../publication/publication.service';
import {PublicationPromoBlockCarouselComponent} from '../publication-promo-block-carousel/publication-promo-block-carousel.component';

@Component({
  selector: 'app-publication-promo-block-quote',
  templateUrl: './publication-promo-block-quote.component.html',
  styleUrls: [
    '../publication-promo-block/publication-promo-block.component.less',
    '../publication-promo-block-carousel/publication-promo-block-carousel.component.less',
    './publication-promo-block-quote.component.less',
  ],
  providers: [PublicationService],
})
export class PublicationPromoBlockQuoteComponent extends PublicationPromoBlockCarouselComponent {
}
