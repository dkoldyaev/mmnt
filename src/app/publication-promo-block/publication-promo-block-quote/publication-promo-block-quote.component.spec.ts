import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicationPromoBlockQuoteComponent } from './publication-promo-block-quote.component';

describe('PublicationPromoBlockQuoteComponent', () => {
  let component: PublicationPromoBlockQuoteComponent;
  let fixture: ComponentFixture<PublicationPromoBlockQuoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicationPromoBlockQuoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicationPromoBlockQuoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
