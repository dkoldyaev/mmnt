import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicationPromoBlockUnderMenuComponent } from './publication-promo-block-under-menu.component';

describe('PublicationPromoBlockUnderMenuComponent', () => {
  let component: PublicationPromoBlockUnderMenuComponent;
  let fixture: ComponentFixture<PublicationPromoBlockUnderMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicationPromoBlockUnderMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicationPromoBlockUnderMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
