import { Component, OnInit } from '@angular/core';
import {PublicationPromoBlockComponent} from '../publication-promo-block/publication-promo-block.component';
import {PublicationService} from '../../publication/publication.service';

@Component({
  selector: 'app-publication-promo-block-under-menu',
  templateUrl: './publication-promo-block-under-menu.component.html',
  styleUrls: [
    '../publication-promo-block/publication-promo-block.component.less',
    './publication-promo-block-under-menu.component.less',
  ],
  providers: [PublicationService],
})
export class PublicationPromoBlockUnderMenuComponent extends PublicationPromoBlockComponent {

}
