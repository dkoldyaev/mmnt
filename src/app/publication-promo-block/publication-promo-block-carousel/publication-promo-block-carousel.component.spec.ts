import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicationPromoBlockCarouselComponent } from './publication-promo-block-carousel.component';

describe('PublicationPromoBlockCarouselComponent', () => {
  let component: PublicationPromoBlockCarouselComponent;
  let fixture: ComponentFixture<PublicationPromoBlockCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicationPromoBlockCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicationPromoBlockCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
