import {Component, Input, OnInit} from '@angular/core';
import {PublicationPromoBlockComponent} from '../publication-promo-block/publication-promo-block.component';
import {NguCarousel} from '@ngu/carousel';
import {PublicationService} from '../../publication/publication.service';

@Component({
  selector: 'app-publication-promo-block-carousel',
  templateUrl: './publication-promo-block-carousel.component.html',
  styleUrls: [
    '../publication-promo-block/publication-promo-block.component.less',
    './publication-promo-block-carousel.component.less',
  ],
  providers: [PublicationService],
})
export class PublicationPromoBlockCarouselComponent extends PublicationPromoBlockComponent {

  carousel: boolean = true;

  @Input()
  public carousel_config: NguCarousel = {
    grid: {xs: 1, sm: 1, md: 1, lg: 1, all: 0},
    slide: 1,
    speed: 400,
    interval: 4000,
    point: {
      visible: false
    },
    load: 2
  };

}
