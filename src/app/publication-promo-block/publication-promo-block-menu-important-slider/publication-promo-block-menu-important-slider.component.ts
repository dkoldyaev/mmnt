import { Component, OnInit } from '@angular/core';
import {PublicationService} from '../../publication/publication.service';
import {PublicationPromoBlockCarouselComponent} from '../publication-promo-block-carousel/publication-promo-block-carousel.component';

@Component({
  selector: 'app-publication-promo-block-menu-important-slider',
  templateUrl: './publication-promo-block-menu-important-slider.component.html',
  styleUrls: [
    '../publication-promo-block/publication-promo-block.component.less',
    './publication-promo-block-menu-important-slider.component.less'
  ],
  providers: [PublicationService],
})
export class PublicationPromoBlockMenuImportantSliderComponent extends PublicationPromoBlockCarouselComponent {

}
