import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicationPromoBlockMenuImportantSliderComponent } from './publication-promo-block-menu-important-slider.component';

describe('PublicationPromoBlockMenuImportantSliderComponent', () => {
  let component: PublicationPromoBlockMenuImportantSliderComponent;
  let fixture: ComponentFixture<PublicationPromoBlockMenuImportantSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicationPromoBlockMenuImportantSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicationPromoBlockMenuImportantSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
