import {Component, Input, OnInit} from '@angular/core';
import {PublicationService} from '../../publication/publication.service';
import {IPublication, Publication} from '../../publication/publication';

@Component({
  selector: 'app-publication-promo-block',
  templateUrl: './publication-promo-block.component.html',
  styleUrls: ['./publication-promo-block.component.less'],
  providers: [PublicationService]
})
export class PublicationPromoBlockComponent implements OnInit {

  @Input() public type: string;
  @Input() public title: string;
  public publications: Publication[] = [];

  constructor(private publication_service: PublicationService) { }

  ngOnInit() {

    const publication_list_observe = this.publication_service.getList({
      'promo': this.type
    });

    publication_list_observe.subscribe(
      (data: {'objects': IPublication[], 'meta': any}) => {
        for (let object_i in data.objects) {
          let new_publication = new Publication(data.objects[object_i]);
          this.publications.push(new_publication);
        }
      }
    );
  }

}
