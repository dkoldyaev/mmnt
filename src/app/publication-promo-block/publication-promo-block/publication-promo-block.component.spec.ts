import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicationPromoBlockComponent } from './publication-promo-block.component';

describe('PublicationPromoBlockComponent', () => {
  let component: PublicationPromoBlockComponent;
  let fixture: ComponentFixture<PublicationPromoBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicationPromoBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicationPromoBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
