import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicationPromoBlockComponent } from './publication-promo-block/publication-promo-block.component';
import { PublicationPromoBlockUnderMenuComponent } from './publication-promo-block-under-menu/publication-promo-block-under-menu.component';
import {
  PublicationPromoBlockOneWrapLineComponent
} from './publication-promo-block-one-wrap-line/publication-promo-block-one-wrap-line.component';
import {
  PublicationPromoBlockMenuImportantSliderComponent
} from './publication-promo-block-menu-important-slider/publication-promo-block-menu-important-slider.component';
import { PublicationPromoBlockQuoteComponent } from './publication-promo-block-quote/publication-promo-block-quote.component';
import { PublicationPromoBlockCarouselComponent } from './publication-promo-block-carousel/publication-promo-block-carousel.component';
import {NguCarouselModule} from '@ngu/carousel';
import {PublicationService} from '../publication/publication.service';
import {SlickModule} from 'ngx-slick';
import {PublicationAnnounceModule} from '../publication-announce/publication-announce.module';

@NgModule({
  imports: [
    CommonModule,
    NguCarouselModule,
    SlickModule,
    PublicationAnnounceModule,
  ],
  declarations: [
    PublicationPromoBlockComponent,
    PublicationPromoBlockUnderMenuComponent,
    PublicationPromoBlockOneWrapLineComponent,
    PublicationPromoBlockMenuImportantSliderComponent,
    PublicationPromoBlockQuoteComponent,
    PublicationPromoBlockCarouselComponent
  ],
  exports: [
    PublicationPromoBlockComponent,
    PublicationPromoBlockUnderMenuComponent,
    PublicationPromoBlockOneWrapLineComponent,
    PublicationPromoBlockMenuImportantSliderComponent,
    PublicationPromoBlockQuoteComponent,
    PublicationPromoBlockCarouselComponent
  ],
  providers: [
    PublicationService
  ]
})
export class PublicationPromoBlockModule { }
