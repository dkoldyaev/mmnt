import { Component, OnInit } from '@angular/core';
import {PublicationPromoBlockComponent} from '../publication-promo-block/publication-promo-block.component';
import {PublicationService} from '../../publication/publication.service';

@Component({
  selector: 'app-publication-promo-block-one-wrap-line',
  templateUrl: './publication-promo-block-one-wrap-line.component.html',
  styleUrls: [
    '../publication-promo-block/publication-promo-block.component.less',
    './publication-promo-block-one-wrap-line.component.less'
  ],
  providers: [PublicationService],
})
export class PublicationPromoBlockOneWrapLineComponent extends PublicationPromoBlockComponent {

}
