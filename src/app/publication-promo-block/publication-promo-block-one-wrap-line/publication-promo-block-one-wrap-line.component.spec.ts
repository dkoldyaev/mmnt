import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicationPromoBlockOneWrapLineComponent } from './publication-promo-block-one-wrap-line.component';

describe('PublicationPromoBlockOneWrapLineComponent', () => {
  let component: PublicationPromoBlockOneWrapLineComponent;
  let fixture: ComponentFixture<PublicationPromoBlockOneWrapLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicationPromoBlockOneWrapLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicationPromoBlockOneWrapLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
