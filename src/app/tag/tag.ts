export interface ITag {
  id: number;
  title: string;
  resource_uri: string;
}

export class Tag implements ITag {
  id: number;
  title: string;
  resource_uri: string;

  constructor(object: ITag) {
    this.id = object.id;
    this.title = object.title;
    this.resource_uri = object.resource_uri;
  }
}
