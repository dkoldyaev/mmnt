import {Component, Input, OnInit} from '@angular/core';
import {Rubric} from '../../rubric/rubric';
import {Tag} from '../tag';

@Component({
  selector: 'app-rubric-tags',
  templateUrl: './rubric-tags.component.html',
  styleUrls: ['./rubric-tags.component.less'],
  providers: []
})
export class RubricTagsComponent implements OnInit {

  @Input() public rubric: Rubric;
  @Input() public tags: Tag[] = [];

  constructor() { }

  ngOnInit() {
  }

}
