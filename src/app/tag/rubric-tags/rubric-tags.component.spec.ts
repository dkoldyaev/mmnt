import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RubricTagsComponent } from './rubric-tags.component';

describe('RubricTagsComponent', () => {
  let component: RubricTagsComponent;
  let fixture: ComponentFixture<RubricTagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RubricTagsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RubricTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
