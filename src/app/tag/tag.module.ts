import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TagListComponent } from './tag-list/tag-list.component';
import {TagService} from './tag.service';
import { RubricTagsComponent } from './rubric-tags/rubric-tags.component';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
  ],
  declarations: [
    TagListComponent,
    RubricTagsComponent,
  ],
  providers: [
    TagService
  ],
  exports: [
    TagListComponent,
    RubricTagsComponent,
  ]
})
export class TagModule { }
