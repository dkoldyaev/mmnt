import { DynamicContentModule } from './dynamic-content-module.module';

describe('DynamicContentModule', () => {
  let dynamicContentModuleModule: DynamicContentModule;

  beforeEach(() => {
    dynamicContentModuleModule = new DynamicContentModule();
  });

  it('should create an instance', () => {
    expect(dynamicContentModuleModule).toBeTruthy();
  });
});
