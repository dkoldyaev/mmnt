import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DynamicPartComponent} from './dynamic-part/dynamic-part.component';
import { DynamicContent2Component } from './dynamic-content/dynamic-content.component';
import {DynamicHtmlPComponent} from './--dynamic-components/html-p/html-p.component';
import {DynamicHtmlBlockquoteComponent} from './--dynamic-components/html-blockquote/html-blockquote.component';
import {DynamicHtmlQuestionComponent} from './--dynamic-components/html-question/html-question.component';
import {DynamicHtmlCustomComponent} from './--dynamic-components/html-custom/html-custom.component';
import {
  DynamicHtmlCustomPlaybuzzComponent
} from './--dynamic-components/html-custom-with-scripts/html-custom-playbuzz/html-custom-playbuzz.component';
import {
  DynamicHtmlCustomInstagramComponent
} from './--dynamic-components/html-custom-with-scripts/html-custom-instagram/html-custom-instagram.component';
import {
  DynamicHtmlCustomFacebookComponent
} from './--dynamic-components/html-custom-with-scripts/html-custom-facebook/html-custom-facebook.component';
import {DynamicHtmlCustomVkComponent} from './--dynamic-components/html-custom-with-scripts/html-custom-vk/html-custom-vk.component';
import {DynamicImageWrapComponent} from './--dynamic-components/image-wrap/image-wrap.component';
import {DynamicImageFloatComponent} from './--dynamic-components/image-float/image-float.component';
import {DynamicGallerySliderComponent} from './--dynamic-components/gallery-slider/gallery-slider.component';
import {DynamicGalleryTilesComponent} from './--dynamic-components/gallery-tiles/gallery-tiles.component';
import {DynamicRelatedPublicationComponent} from './--dynamic-components/related-publication/related-publication.component';
import {DynamicEmbedVideoComponent} from './--dynamic-components/embed-video/embed-video.component';
import {DynamicEventListComponent} from './--dynamic-components/event-list/event-list.component';
import {BrowserModule} from '@angular/platform-browser';
import {SlickModule} from 'ngx-slick';
import {RouterModule} from '@angular/router';
import {StickyModule} from 'ng2-sticky-kit';
import {SimpleElementsModule} from '../simple-elements/simple-elements.module';
import {PublicationAnnounceModule} from '../publication-announce/publication-announce.module';
import {EventsModule} from '../events/events.module';
import {SafeResourcePipe} from '../--pipes/safe-resource/safe-resource.pipe';
import {BaseDynamicComponent} from './--dynamic-components/_base-dynamic/base-dynamic.component';
import {DynamicImageComponent} from './--dynamic-components/_image/image.component';
import {DynamicVideoComponent} from './--dynamic-components/_video/video.component';
import {DynamicHtmlCustomWithScriptsComponent} from './--dynamic-components/html-custom-with-scripts/html-custom-with-scripts.component';
import {DynamicGalleryComponent} from './--dynamic-components/gallery/gallery.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    SlickModule,
    RouterModule,
    StickyModule,
    SimpleElementsModule,
    PublicationAnnounceModule,
    EventsModule,
  ],
  declarations: [
    DynamicPartComponent,
    DynamicContent2Component,
    SafeResourcePipe,

    BaseDynamicComponent,
    DynamicHtmlPComponent,
    DynamicHtmlBlockquoteComponent,
    DynamicHtmlQuestionComponent,
    DynamicHtmlCustomComponent,
    DynamicHtmlCustomWithScriptsComponent,
    DynamicHtmlCustomPlaybuzzComponent,
    DynamicHtmlCustomInstagramComponent,
    DynamicHtmlCustomFacebookComponent,
    DynamicHtmlCustomVkComponent,
    DynamicImageComponent,
    DynamicImageWrapComponent,
    DynamicImageFloatComponent,
    DynamicGalleryComponent,
    DynamicGallerySliderComponent,
    DynamicGalleryTilesComponent,
    DynamicRelatedPublicationComponent,
    DynamicVideoComponent,
    DynamicEmbedVideoComponent,
    DynamicEventListComponent,
  ],
  exports: [
    DynamicPartComponent,
    DynamicContent2Component,

    BaseDynamicComponent,
    DynamicHtmlPComponent,
    DynamicHtmlBlockquoteComponent,
    DynamicHtmlQuestionComponent,
    DynamicHtmlCustomComponent,
    DynamicHtmlCustomWithScriptsComponent,
    DynamicHtmlCustomPlaybuzzComponent,
    DynamicHtmlCustomInstagramComponent,
    DynamicHtmlCustomFacebookComponent,
    DynamicHtmlCustomVkComponent,
    DynamicImageComponent,
    DynamicImageWrapComponent,
    DynamicImageFloatComponent,
    DynamicGalleryComponent,
    DynamicGallerySliderComponent,
    DynamicGalleryTilesComponent,
    DynamicRelatedPublicationComponent,
    DynamicVideoComponent,
    DynamicEmbedVideoComponent,
    DynamicEventListComponent,
  ],
  entryComponents: [
    BaseDynamicComponent,
    DynamicHtmlPComponent,
    DynamicHtmlBlockquoteComponent,
    DynamicHtmlQuestionComponent,
    DynamicHtmlCustomComponent,
    DynamicHtmlCustomWithScriptsComponent,
    DynamicHtmlCustomPlaybuzzComponent,
    DynamicHtmlCustomInstagramComponent,
    DynamicHtmlCustomFacebookComponent,
    DynamicHtmlCustomVkComponent,
    DynamicImageComponent,
    DynamicImageWrapComponent,
    DynamicImageFloatComponent,
    DynamicGalleryComponent,
    DynamicGallerySliderComponent,
    DynamicGalleryTilesComponent,
    DynamicRelatedPublicationComponent,
    DynamicVideoComponent,
    DynamicEmbedVideoComponent,
    DynamicEventListComponent,
  ]
})
export class DynamicContentModule { }
