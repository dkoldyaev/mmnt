import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {DynamicHtmlCustom, DynamicHtmlCustomComponent} from '../../html-custom/html-custom.component';
import {ScriptHackComponent} from '../../../../simple-elements/script/script.component';
import {DynamicHtmlCustomWithScriptsComponent} from '../html-custom-with-scripts.component';

@Component({
  selector: 'app-html-custom-playbuzz',
  templateUrl: '../html-custom-with-scripts.component.html',
  styleUrls: ['./html-custom-playbuzz.component.less']
})
export class DynamicHtmlCustomPlaybuzzComponent extends DynamicHtmlCustomWithScriptsComponent implements OnInit {

  @Input()
  public part: DynamicHtmlCustom;

  @ViewChild('customHtmlContainer')
  customHtmlContainer: ElementRef;

  ngOnInit() {
    this.inner_script = `
      (function(d,s,id){
        var js,fjs=d.getElementsByTagName(s)[0];
        if(d.getElementById(id))return;
        js=d.createElement(s);js.id=id;
        js.src='https://embed.playbuzz.com/sdk.js';
        fjs.parentNode.insertBefore(js,fjs);
      }(document,'script','playbuzz-sdk'));`;
    super.ngOnInit();
  }

  escape_code(): string {
    if (!(this.part && this.part.content)) {
      return '';
    }
    let result = super.escape_code()
      .replace(/<script[^>]*>.*?<\/script>/, '');
    return result;
  }

}
