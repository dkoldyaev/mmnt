import {Component, Input, OnInit} from '@angular/core';
import {DynamicHtmlCustomWithScriptsComponent} from '../html-custom-with-scripts.component';

@Component({
  selector: 'app-html-custom-instagram',
  templateUrl: '../html-custom-with-scripts.component.html',
  styleUrls: ['./html-custom-instagram.component.less']
})
export class DynamicHtmlCustomInstagramComponent extends DynamicHtmlCustomWithScriptsComponent implements OnInit {

  @Input()
  src: string = '//www.instagram.com/embed.js';

}
