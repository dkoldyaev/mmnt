import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {ScriptHackComponent} from '../../../simple-elements/script/script.component';
import {DynamicHtmlCustom} from '../html-custom/html-custom.component';
import {RenderService} from '../../../publication/render.service';
import {IsBrowserOrServerService} from '../../../--services/is-browser-or-server.service';

@Component({
  selector: 'app-html-custom-with-scripts',
  templateUrl: './html-custom-with-scripts.component.html',
  styleUrls: ['./html-custom-with-scripts.component.less']
})
export class DynamicHtmlCustomWithScriptsComponent extends ScriptHackComponent implements OnInit, AfterViewInit {

  @Input()
  public part: DynamicHtmlCustom;

  @ViewChild('customHtmlContainer')
  customHtmlContainer: ElementRef;

  constructor(
    protected render_service: RenderService,
    public is_browser_or_server_service: IsBrowserOrServerService,
  ) {
    super(is_browser_or_server_service);
  }

  ngOnInit() {
    this.customHtmlContainer.nativeElement.innerHTML = this.escape_code();
  }

  escape_code(): string {
    if (!(this.part && this.part.content)) {
      return '';
    }
    return this.part.content
      .replace(/^<div[^>]*class="[^"]*custom\-html[^"]*"[^>]*>/gm, '')
      .replace(/<\/div>$/, '');
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
    this.render_service.element_ready.emit(true);
  }

}
