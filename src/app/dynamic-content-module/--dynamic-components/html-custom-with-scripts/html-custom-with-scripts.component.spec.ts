import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicHtmlCustomWithScriptsComponent } from './html-custom-with-scripts.component';

describe('DynamicHtmlCustomWithScriptsComponent', () => {
  let component: DynamicHtmlCustomWithScriptsComponent;
  let fixture: ComponentFixture<DynamicHtmlCustomWithScriptsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicHtmlCustomWithScriptsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicHtmlCustomWithScriptsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
