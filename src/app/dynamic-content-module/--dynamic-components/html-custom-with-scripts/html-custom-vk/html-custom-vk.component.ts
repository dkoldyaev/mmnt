import { Component, OnInit } from '@angular/core';
import {DynamicHtmlCustomWithScriptsComponent} from '../html-custom-with-scripts.component';

@Component({
  selector: 'app-html-custom-vk',
  templateUrl: '../html-custom-with-scripts.component.html',
  styleUrls: ['./html-custom-vk.component.less']
})
export class DynamicHtmlCustomVkComponent extends DynamicHtmlCustomWithScriptsComponent implements OnInit {

  escape_code(): string {
    if (!(this.part && this.part.content)) {
      return '';
    }
    let vk_data = this.part.content.match(/<script[^>]*>(.*)<\/script>/);
    this.inner_script = vk_data[1];
    let result = super.escape_code()
      .replace(/<script[^>]*>.*?<\/script>/, '');
    return result;
  }

}
