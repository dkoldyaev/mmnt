import {Component, Input, OnInit} from '@angular/core';
import {DynamicHtmlCustomWithScriptsComponent} from '../html-custom-with-scripts.component';
import {DynamicHtmlCustom, DynamicHtmlCustomComponent} from '../../html-custom/html-custom.component';

@Component({
  selector: 'app-html-custom-facebook',
  templateUrl: '../../html-custom/html-custom.component.html',
  styleUrls: ['./html-custom-facebook.component.less']
})
export class DynamicHtmlCustomFacebookComponent extends DynamicHtmlCustomComponent implements OnInit {

  escape_code(): string {
    if (!(this.part && this.part.content)) {
      return '';
    }
    let result = this.part.content
      .replace(/<div class="custom\-html[^"]*">(<div class="fb\-post"[^>]+><\/div>)<\/div>/, '$1');
    return result;
  }

}
