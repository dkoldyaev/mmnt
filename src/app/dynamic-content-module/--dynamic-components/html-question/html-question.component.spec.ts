import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicHtmlQuestionComponent } from './html-question.component';

describe('DynamicHtmlPComponent', () => {
  let component: DynamicHtmlQuestionComponent;
  let fixture: ComponentFixture<DynamicHtmlQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicHtmlQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicHtmlQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
