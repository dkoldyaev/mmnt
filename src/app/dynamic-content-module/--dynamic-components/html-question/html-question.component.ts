import {Component, Input, OnInit} from '@angular/core';
import {DynamicInterface} from '../../dynamic.interface';
import {BaseDynamicComponent} from '../_base-dynamic/base-dynamic.component';

export class DynamicHtmlQuestion implements DynamicInterface {
  content: string;
  type: string = 'html-question';
}

@Component({
  selector: 'app-dynamic-html-question',
  templateUrl: './html-question.component.html',
  styleUrls: ['./html-question.component.less']
})
export class DynamicHtmlQuestionComponent extends BaseDynamicComponent {

  @Input()
  public part: DynamicHtmlQuestion;

}
