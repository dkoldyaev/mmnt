import {Component, ElementRef, HostListener, Input, OnInit, ViewChild} from '@angular/core';
import {DynamicInterface} from '../../dynamic.interface';
import {BaseDynamicComponent} from '../_base-dynamic/base-dynamic.component';
import {IsBrowserOrServerService} from '../../../--services/is-browser-or-server.service';
import {RenderService} from '../../../publication/render.service';

export interface IDynamicHtmlCustom extends DynamicInterface {
  type: string;
  content: string;
}

export class DynamicHtmlCustom implements IDynamicHtmlCustom {
  type: string = 'html-custom';
  content: string;
}

@Component({
  selector: 'app-dynamic-html-custom',
  templateUrl: './html-custom.component.html',
  styleUrls: ['./html-custom.component.less']
})
export class DynamicHtmlCustomComponent extends BaseDynamicComponent implements OnInit {

  @Input()
  public part: DynamicHtmlCustom;

  @ViewChild('customHtmlContainer')
  customHtmlContainer: ElementRef;

  private iframe_body: Element;
  public current_height: string = 'auto';

  constructor(
    protected render_service: RenderService,
    protected is_browser_or_server_service: IsBrowserOrServerService
  ) {
    super(render_service);
  }

  escape_code() {
    if (!(this.part && this.part.content)) {
      return '';
    }
    let result = this.part.content
      .replace(/^<div[^>]*class="[^"]*custom\-html[^"]*"[^>]*>/gm, '')
      .replace(/<\/div>$/, '');
    return result;
  }

  ngOnInit() {
    this.customHtmlContainer.nativeElement.innerHTML = this.escape_code();
    if (this.is_browser_or_server_service.is_browser) {
      setInterval(
        () => {
          this.calcHeight();
        },
        300
      );
    }
  }

  @HostListener('window:resize', [])
  private calcHeight() {
    if (this.is_browser_or_server_service.is_server) {
      return;
    }
    try {
      this.current_height = '' + this.customHtmlContainer.nativeElement.firstChild.contentWindow.document.body.offsetHeight + 'px';
      if (!this.current_height) {
        throw new Error();
      }
    } catch (e) {
      this.current_height = 'auto';
    }
  }

}
