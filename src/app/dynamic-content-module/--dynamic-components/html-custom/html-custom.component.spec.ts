import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicHtmlCustomComponent } from './html-custom.component';

describe('DynamicHtmlCustomComponent', () => {
  let component: DynamicHtmlCustomComponent;
  let fixture: ComponentFixture<DynamicHtmlCustomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicHtmlCustomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicHtmlCustomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
