import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {DynamicEmbedVideoComponent} from './embed-video.component';

describe('EmbedVideoComponent', () => {
  let component: DynamicEmbedVideoComponent;
  let fixture: ComponentFixture<DynamicEmbedVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicEmbedVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicEmbedVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
