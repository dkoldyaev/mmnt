import {Component, Input, OnInit} from '@angular/core';
import {DynamicInterface} from '../../dynamic.interface';
import {Video} from '../_video/video.component';
import {DomSanitizer} from '@angular/platform-browser';
import {BaseDynamicComponent} from '../_base-dynamic/base-dynamic.component';

class DynamicEmbedVideo implements DynamicInterface {
  type: string = 'embed-video';
  video: Video;
}

@Component({
  selector: 'app-dynamic-embed-video',
  templateUrl: './embed-video.component.html',
  styleUrls: ['./embed-video.component.less']
})
export class DynamicEmbedVideoComponent extends BaseDynamicComponent {

  @Input()
  public part: DynamicEmbedVideo;

}
