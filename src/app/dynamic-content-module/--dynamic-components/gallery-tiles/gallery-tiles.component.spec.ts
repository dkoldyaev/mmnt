import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicGalleryTilesComponent } from './gallery-tiles.component';

describe('DynamicGalleryTilesComponent', () => {
  let component: DynamicGalleryTilesComponent;
  let fixture: ComponentFixture<DynamicGalleryTilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicGalleryTilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicGalleryTilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
