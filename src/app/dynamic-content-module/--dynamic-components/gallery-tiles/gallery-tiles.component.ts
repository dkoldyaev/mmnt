import {AfterViewInit, Component, HostListener, Inject, Input, OnInit, PLATFORM_ID} from '@angular/core';
import {DynamicGalery, DynamicGalleryComponent} from '../gallery/gallery.component';
import {isNull} from 'util';
import {RenderService} from '../../../publication/render.service';
import {ShadowService} from '../../../--services/shadow.service';
import {IsBrowserOrServerService} from '../../../--services/is-browser-or-server.service';

@Component({
  selector: 'app-dynamic-gallery-tiles',
  templateUrl: './gallery-tiles.component.html',
  styleUrls: ['./gallery-tiles.component.less']
})

export class DynamicGalleryTilesComponent extends DynamicGalleryComponent implements OnInit, AfterViewInit {

  constructor(
    protected render_service: RenderService,
    protected shadow_service: ShadowService,
    public is_browser_or_server_service: IsBrowserOrServerService,
  ) {
    super(render_service, is_browser_or_server_service);
  }

  public current_image_number: number = null;

  public ngOnInit() {
    this.shadow_service.onShadowShow.subscribe((shadow_showed) => {
      if (!shadow_showed) {
        this._hidePopUp();
      }
    });
  }

  showPopUp(image_number: number) {
    this._showPopUp(image_number);
    this.shadowStateChanged.emit(true);
  }

  hidePopUp($event?) {
    if ($event) {
      $event.preventDefault();
    }
    this._hidePopUp();
    this.shadowStateChanged.emit(false);
  }

  private _hidePopUp() {
    this.current_image_number = null;
  }

  private _showPopUp(image_number: number) {
    if (image_number < this.part.images.length) {
      this.current_image_number = image_number;
    }
  }

  public popUpShowed(): boolean {
    return !isNull(this.current_image_number);
  }

  nextPopUp($event?) {
    if ($event) {
      $event.preventDefault();
    }
    this._nextPopUp();
  }

  private _nextPopUp() {
    if (!this.popUpShowed()) {
      return;
    }
    this.current_image_number = this.current_image_number < this.part.images.length - 1
      ? this.current_image_number + 1
      : 0;
  }

  prevPopUp($event?) {
    if ($event) {
      $event.preventDefault();
    }
    this._prevPopUp();
  }

  private _prevPopUp() {
    if (!this.popUpShowed()) {
      return;
    }
    this.current_image_number = this.current_image_number === 0
      ? this.part.images.length - 1
      : this.current_image_number - 1;
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (!this.popUpShowed()) {
      return;
    }
    switch (event.key) {
      case 'ArrowRight':
        this.nextPopUp();
        break;
      case 'ArrowLeft':
        this.prevPopUp();
        break;
      case 'Escape':
        break;
      default:
      // console.log(event);
    }
  }

}
