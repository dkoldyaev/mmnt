import { Component, OnInit } from '@angular/core';
import {DynamicInterface} from '../../dynamic.interface';
import {IMage} from '../../../simple-elements/image/image';

export class DynamicImage implements DynamicInterface {
  type: string;
  image: IMage;
}

@Component({
  selector: 'app-dynamic-image--abstract',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.less']
})
export class DynamicImageComponent {
}
