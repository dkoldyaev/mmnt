import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicImageFloatComponent } from './image-float.component';

describe('DynamicImageFloatComponent', () => {
  let component: DynamicImageFloatComponent;
  let fixture: ComponentFixture<DynamicImageFloatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicImageFloatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicImageFloatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
