import {Component, Input, OnInit} from '@angular/core';
import {DynamicImage, DynamicImageComponent} from '../_image/image.component';
import {BaseDynamicComponent} from '../_base-dynamic/base-dynamic.component';

export class DynamicImageFloat extends DynamicImage {
  position: string;
  type: string = '_image-float';
}

@Component({
  selector: 'app-dynamic-image-float',
  templateUrl: './image-float.component.html',
  styleUrls: [
    '../_image/image.component.less',
    './image-float.component.less'
  ]
})
export class DynamicImageFloatComponent extends BaseDynamicComponent {

  @Input()
  public part: DynamicImageFloat;

}
