import {Component, Input, OnInit} from '@angular/core';
import {DynamicInterface} from '../../dynamic.interface';
import {BaseDynamicComponent} from '../_base-dynamic/base-dynamic.component';

export class DynamicHtmlP implements DynamicInterface {
  content: string;
  type: string = 'html-p';
}

@Component({
  selector: 'app-dynamic-html-p',
  templateUrl: './html-p.component.html',
  styleUrls: ['./html-p.component.less']
})
export class DynamicHtmlPComponent extends BaseDynamicComponent {

  @Input()
  public part: DynamicHtmlP;

}
