import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicHtmlPComponent } from './html-p.component';

describe('DynamicHtmlPComponent', () => {
  let component: DynamicHtmlPComponent;
  let fixture: ComponentFixture<DynamicHtmlPComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicHtmlPComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicHtmlPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
