import {AfterViewInit, Component, OnInit} from '@angular/core';
import {DynamicGalleryComponent} from '../gallery/gallery.component';

declare const jquery: any;   // not required
declare const $: any;   // not required

@Component({
  selector: 'app-dynamic-gallery-slider',
  templateUrl: './gallery-slider.component.html',
  styleUrls: [
    './gallery-slider.component.less',
  ],
  providers: [],
})
export class DynamicGallerySliderComponent extends DynamicGalleryComponent implements OnInit, AfterViewInit {

  public current_slide: number = 0;

  public ngOnInit() {
  }

  afterChange(e) {
    this.current_slide = e.currentSlide;
  }

}
