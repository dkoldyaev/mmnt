import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicGallerySliderComponent } from './gallery-slider.component';

describe('DynamicGallerySliderComponent', () => {
  let component: DynamicGallerySliderComponent;
  let fixture: ComponentFixture<DynamicGallerySliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicGallerySliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicGallerySliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
