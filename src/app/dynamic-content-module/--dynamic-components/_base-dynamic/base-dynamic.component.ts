import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RenderService} from '../../../publication/render.service';
import {ShadowService} from '../../../--services/shadow.service';
import {DynamicInterface} from '../../dynamic.interface';

@Component({
  selector: 'app-base-dynamic',
  template: ' ',
})
export class BaseDynamicComponent implements OnInit, AfterViewInit {

  @Output()
  public shadowStateChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input()
  public part: DynamicInterface;

  constructor(
    protected render_service: RenderService
  ) {}

  ready() {
    this.render_service.element_ready.emit(true);
  }

  ngAfterViewInit() {
    this.ready();
  }

  ngOnInit() {
  }

}
