import {AfterViewInit, Component, HostListener, Input, OnInit} from '@angular/core';
import {EventsService} from '../../../events/events.service';
import {DynamicInterface} from '../../dynamic.interface';
import {EVent} from '../../../events/event';
import * as moment from 'moment';
import {BaseDynamicComponent} from '../_base-dynamic/base-dynamic.component';
import {RenderService} from '../../../publication/render.service';
import {IsBrowserOrServerService} from '../../../--services/is-browser-or-server.service';

export class DynamicEventList implements DynamicInterface {
  type: string = 'event-list';
  event_ids: number[] = [];
}

@Component({
  selector: 'app-dynamic-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.less'],
  providers: [
    EventsService,
  ]
})
export class DynamicEventListComponent extends BaseDynamicComponent implements OnInit, AfterViewInit {

  @Input()
  public part: DynamicEventList;

  public dates: {date: Date|string, events: EVent[]}[] = [];
  public load_end: boolean = false;
  public header_offset: number = 0;

  private events: EVent[] = [];
  private input_count: number = 0;
  private render_count: number = 0;
  private rendered: boolean = false;

  constructor(
    private event_service: EventsService,
    protected render_service: RenderService,
    public is_browser_or_server_service: IsBrowserOrServerService,
  ) {
    super(render_service);
  }

  ngOnInit() {
    this.input_count = this.part.event_ids.length;
    this.part.event_ids.forEach((id, i, event_ids) => {
      this.event_service.getDetail(+id).subscribe(
        (data) => {
          this.events[i] = new EVent(data);
          this.render_count++;
        },
        (error) => {
          this.input_count--;
        },
        () => {
          this.tryAllRendered();
        }
      );
    });
  }

  tryAllRendered() {
    if (this.render_count === this.input_count && !this.rendered) {
      this.render_events();
    }
  }

  private render_events() {
    this.events.sort((event_1, event_2) => {
      if (event_1.date_start > event_2.date_start) {
        return 1;
      } else if (event_1.date_start < event_2.date_start) {
        return -1;
      }
      return 0;
    });
    let current_date: string = null;
    let curent_date_number = -1;
    for (let event_i in this.events) {
      let current_event = this.events[event_i];
      let event_date = moment(current_event.date_start).format('YYYYMMDD');
      if (!current_date || event_date !== current_date) {
        curent_date_number++;
        current_date = event_date;
        this.dates[curent_date_number] = {
          date: current_event.date_start,
          events: []
        };
      }
      this.dates[curent_date_number].events.push(current_event);
    }
    this.rendered = true;
    this.ready();
  }

  ngAfterViewInit() {}

}
