import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicEventListComponent } from './event-list.component';

describe('DynamicEventListComponent', () => {
  let component: DynamicEventListComponent;
  let fixture: ComponentFixture<DynamicEventListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicEventListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicEventListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
