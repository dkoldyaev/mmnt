import {Component, Input, OnInit} from '@angular/core';
import {DynamicInterface} from '../../dynamic.interface';
import {DynamicImage, DynamicImageComponent} from '../_image/image.component';
import {BaseDynamicComponent} from '../_base-dynamic/base-dynamic.component';

export class DynamicImageWrap extends DynamicImage {
  type: string = '_image-wrap';
}

@Component({
  selector: 'app-dynamic-image-wrap',
  templateUrl: './image-wrap.component.html',
  styleUrls: [
    '../_image/image.component.less',
    './image-wrap.component.less'
  ]
})
export class DynamicImageWrapComponent extends BaseDynamicComponent {

  @Input()
  public part: DynamicImageWrap;

}
