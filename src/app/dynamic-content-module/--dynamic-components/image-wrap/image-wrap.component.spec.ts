import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicImageWrapComponent } from './image-wrap.component';

describe('DynamicImageWrapComponent', () => {
  let component: DynamicImageWrapComponent;
  let fixture: ComponentFixture<DynamicImageWrapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicImageWrapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicImageWrapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
