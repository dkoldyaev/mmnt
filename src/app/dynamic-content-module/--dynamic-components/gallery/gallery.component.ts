import {AfterViewInit, Component, EventEmitter, HostListener, Inject, Input, OnInit, Output, PLATFORM_ID, ViewChild} from '@angular/core';
import {DynamicInterface} from '../../dynamic.interface';
import {IMage} from '../../../simple-elements/image/image';
import {getRandomString} from 'selenium-webdriver/safari';
import {BaseDynamicComponent} from '../_base-dynamic/base-dynamic.component';
import {RenderService} from '../../../publication/render.service';
import {ShadowService} from '../../../--services/shadow.service';
import {isNull} from 'util';
import {isPlatformBrowser} from '@angular/common';
import {IsBrowserOrServerService} from '../../../--services/is-browser-or-server.service';

declare const jquery: any;   // not required
declare const $: any;   // not required

export class DynamicGalery implements DynamicInterface {
  type: string = 'gallery';
  images: IMage[] = [];
  format?: string;
}

@Component({
  selector: 'app-dynamic-gallery',
  template: ' ',
  styleUrls: [],
  providers: [],
})
export class DynamicGalleryComponent extends BaseDynamicComponent implements OnInit, AfterViewInit {

  @Input()
  public part: DynamicGalery;

  constructor(
    protected render_service: RenderService,
    public is_browser_or_server_service: IsBrowserOrServerService
  ) {
    super(render_service);
  }

}
