import {Component, Input, OnInit} from '@angular/core';
import {DynamicInterface} from '../../dynamic.interface';
import {BaseDynamicComponent} from '../_base-dynamic/base-dynamic.component';

export class DynamicHtmlBlockquote implements DynamicInterface {
  content: string;
  author: string;
  type: string = 'html-blockquote';
}

@Component({
  selector: 'app-dynamic-html-blockquote',
  templateUrl: './html-blockquote.component.html',
  styleUrls: ['./html-blockquote.component.less']
})
export class DynamicHtmlBlockquoteComponent extends BaseDynamicComponent {

  @Input()
  public part: DynamicHtmlBlockquote;

}
