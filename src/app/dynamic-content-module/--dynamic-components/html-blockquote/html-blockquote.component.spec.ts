import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicHtmlBlockquoteComponent } from './html-blockquote.component';

describe('DynamicHtmlPComponent', () => {
  let component: DynamicHtmlBlockquoteComponent;
  let fixture: ComponentFixture<DynamicHtmlBlockquoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicHtmlBlockquoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicHtmlBlockquoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
