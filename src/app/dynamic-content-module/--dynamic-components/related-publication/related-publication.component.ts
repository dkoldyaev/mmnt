import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {DynamicInterface} from '../../dynamic.interface';
import {IPublication, Publication} from '../../../publication/publication';
import {PublicationAnnounceComponent} from '../../../publication-announce/publication-announce-component/publication-announce.component';
import {PublicationService} from '../../../publication/publication.service';
import {RenderService} from '../../../publication/render.service';
import {BaseDynamicComponent} from '../_base-dynamic/base-dynamic.component';

export class DynamicRelatedPublication implements DynamicInterface {
  type: string = 'related-publication';
  publication: Publication;
  position?: string;
  content: string;
}

@Component({
  selector: 'app-dynamic-related-publication',
  templateUrl: './related-publication.component.html',
  styleUrls: [
    './related-publication.component.less',
  ],
  providers: [
    PublicationService
  ]
})
export class DynamicRelatedPublicationComponent extends BaseDynamicComponent implements OnInit {

  @Input()
  public part: DynamicRelatedPublication;

  constructor(
    private publication_service: PublicationService,
    protected render_service: RenderService
  ) {
    super(render_service);
  }

  ngOnInit() {
    if (!this.part.publication || !this.part.publication.announce_image || !this.part.publication.title) {
      this.publication_service.getDetail(this.part.publication.id).subscribe(
        (data: IPublication) => {
          this.part.publication = new Publication(data);
        },
        (error) => {
          this.part = null;
        }
      );
    } else {
      this.part.publication = new Publication(this.part.publication);
    }
  }

}
