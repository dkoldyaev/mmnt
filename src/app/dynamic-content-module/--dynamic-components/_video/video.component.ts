import { Component, OnInit } from '@angular/core';
import {BaseDynamicComponent} from '../_base-dynamic/base-dynamic.component';

export class Video {
  source: string;
  host: string;
}

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.less']
})
export class DynamicVideoComponent extends BaseDynamicComponent implements OnInit {

  ngOnInit() {
  }

}
