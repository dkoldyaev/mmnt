import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicVideoComponent } from './video.component';

describe('DynamicVideoComponent', () => {
  let component: DynamicVideoComponent;
  let fixture: ComponentFixture<DynamicVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
