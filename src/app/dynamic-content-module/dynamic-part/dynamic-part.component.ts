import {Component, ComponentFactoryResolver, ComponentRef, Input, OnDestroy, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {DynamicInterface} from '../dynamic.interface';
import {DynamicHtmlPComponent} from '../--dynamic-components/html-p/html-p.component';
import {DynamicHtmlBlockquoteComponent} from '../--dynamic-components/html-blockquote/html-blockquote.component';
import {DynamicHtmlQuestionComponent} from '../--dynamic-components/html-question/html-question.component';
import {DynamicHtmlCustomComponent} from '../--dynamic-components/html-custom/html-custom.component';
import {
  DynamicHtmlCustomPlaybuzzComponent
} from '../--dynamic-components/html-custom-with-scripts/html-custom-playbuzz/html-custom-playbuzz.component';
import {
  DynamicHtmlCustomInstagramComponent
} from '../--dynamic-components/html-custom-with-scripts/html-custom-instagram/html-custom-instagram.component';
import {
  DynamicHtmlCustomFacebookComponent
} from '../--dynamic-components/html-custom-with-scripts/html-custom-facebook/html-custom-facebook.component';
import {DynamicHtmlCustomVkComponent} from '../--dynamic-components/html-custom-with-scripts/html-custom-vk/html-custom-vk.component';
import {DynamicImageWrapComponent} from '../--dynamic-components/image-wrap/image-wrap.component';
import {DynamicImageFloatComponent} from '../--dynamic-components/image-float/image-float.component';
import {DynamicGallerySliderComponent} from '../--dynamic-components/gallery-slider/gallery-slider.component';
import {DynamicGalleryTilesComponent} from '../--dynamic-components/gallery-tiles/gallery-tiles.component';
import {DynamicRelatedPublicationComponent} from '../--dynamic-components/related-publication/related-publication.component';
import {DynamicEmbedVideoComponent} from '../--dynamic-components/embed-video/embed-video.component';
import {DynamicEventListComponent} from '../--dynamic-components/event-list/event-list.component';
import {BaseDynamicComponent} from '../--dynamic-components/_base-dynamic/base-dynamic.component';
import {ShadowService} from '../../--services/shadow.service';

@Component({
  selector: 'app-dynamic-part',
  templateUrl: './dynamic-part.component.html',
  styleUrls: ['./dynamic-part.component.less']
})
export class DynamicPartComponent implements OnInit, OnDestroy {

  @ViewChild('container', { read: ViewContainerRef })
  container: ViewContainerRef;

  private componentRef: ComponentRef<{}>;

  @Input()
  part: DynamicInterface;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    protected shadow_service: ShadowService
  ) {}

  getComponentClass(text_part: DynamicInterface): any {

    if (!text_part.type) {
      throw new Error(`Неправильные данные. Нет типа`);
    }

    if (text_part.type === 'html-custom' && text_part.hasOwnProperty('content')) {
      if (/class="playbuzz"/g.test(text_part['content'])) {
        text_part.type = text_part.type + '-playbuzz';
      } else if (/class="instagram\-media"/g.test(text_part['content'])) {
        text_part.type = text_part.type + '-instagram';
      } else if (/class="fb\-post"/g.test(text_part['content'])) {
        text_part.type = text_part.type + '-facebook';
      } else if (/id="vk_post_\d+_\d+"/g.test(text_part['content'])) {
        text_part.type = text_part.type + '-vk';
      }
    }

    switch (text_part.type) {

      case 'html-p':
        return DynamicHtmlPComponent;

      case 'html-blockquote':
        return DynamicHtmlBlockquoteComponent;

      case 'html-question':
        return DynamicHtmlQuestionComponent;

      case 'html-custom':
        return DynamicHtmlCustomComponent;

      case 'html-custom-playbuzz':
        return DynamicHtmlCustomPlaybuzzComponent;

      case 'html-custom-instagram':
        return DynamicHtmlCustomInstagramComponent;

      case 'html-custom-facebook':
        return DynamicHtmlCustomFacebookComponent;

      case 'html-custom-vk':
        return DynamicHtmlCustomVkComponent;

      case 'image-wrap':
        return DynamicImageWrapComponent;

      case 'image-float':
        return DynamicImageFloatComponent;

      case 'gallery':
        switch (text_part['format']) {
          case 'slider':
            return DynamicGallerySliderComponent;

          case 'tiles':
          default:
            return DynamicGalleryTilesComponent;
        }

      case 'related-publication':
        return DynamicRelatedPublicationComponent;

      case 'embed-video':
        return DynamicEmbedVideoComponent;

      case 'event-list':
        return DynamicEventListComponent;

      default:
        throw new Error(`Рендер для ${text_part.type} еще не написан`);
    }

  }

  ngOnInit() {
    let component_type = this.getComponentClass(this.part);
    let factory = this.componentFactoryResolver.resolveComponentFactory(component_type);
    this.componentRef = this.container.createComponent(factory);
    this.componentRef.instance['part'] = this.part;
    this.componentRef.instance['shadowStateChanged'].subscribe((data: boolean) => {
      this.shadow_service.onShadowShow.emit(data);
    });
  }

  ngOnDestroy() {
    if (this.componentRef) {
      this.componentRef.destroy();
      this.componentRef = null;
    }
  }

}
