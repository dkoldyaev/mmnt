import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicPartComponent } from './dynamic-part.component';

describe('DynamicPartComponent', () => {
  let component: DynamicPartComponent;
  let fixture: ComponentFixture<DynamicPartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicPartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicPartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
