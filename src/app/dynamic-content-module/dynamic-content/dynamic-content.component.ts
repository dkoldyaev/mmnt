import {Component, Input, OnInit} from '@angular/core';
import {DynamicInterface} from '../dynamic.interface';

@Component({
  selector: 'app-dynamic-content-2',
  templateUrl: './dynamic-content.component.html',
  styleUrls: ['./dynamic-content.component.less']
})
export class DynamicContent2Component implements OnInit {

  @Input()
  text_parts: DynamicInterface[];

  constructor() { }

  ngOnInit() {
  }

}
