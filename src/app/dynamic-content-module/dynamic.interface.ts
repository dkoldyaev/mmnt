export interface DynamicInterface {
  type: string;
  content?: string;
}
