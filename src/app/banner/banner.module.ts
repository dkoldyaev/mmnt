import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BannerComponent } from './banner/banner.component';
import {BannerService} from './banner.service';
import { BannerAdfoxComponent } from './banner-adfox/banner-adfox.component';
import { BannerTelegramSubscribeComponent } from './banner-telegram-subscribe/banner-telegram-subscribe.component';
import {SimpleElementsModule} from '../simple-elements/simple-elements.module';

@NgModule({
  imports: [
    CommonModule,
    SimpleElementsModule,
  ],
  declarations: [
    BannerComponent,
    BannerAdfoxComponent,
    BannerTelegramSubscribeComponent,
  ],
  exports: [
    BannerComponent,
    BannerAdfoxComponent,
    BannerTelegramSubscribeComponent,
  ],
  providers: [
    BannerService
  ]
})
export class BannerModule { }
