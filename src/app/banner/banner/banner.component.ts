import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BannerService} from '../banner.service';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.less']
})
export class BannerComponent implements OnInit {

  @Input()
  public image_url: string;

  @Input()
  public link: string = '';

  @Output()
  public banner_show: EventEmitter<boolean> = new EventEmitter();

  constructor() {}

  ngOnInit() {
    this.banner_show.emit(!!this.image_url);
  }

}
