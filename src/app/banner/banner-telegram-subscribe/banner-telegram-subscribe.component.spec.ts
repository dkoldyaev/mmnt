import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerTelegramSubscribeComponent } from './banner-telegram-subscribe.component';

describe('BannerTelegramSubscribeComponent', () => {
  let component: BannerTelegramSubscribeComponent;
  let fixture: ComponentFixture<BannerTelegramSubscribeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BannerTelegramSubscribeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerTelegramSubscribeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
