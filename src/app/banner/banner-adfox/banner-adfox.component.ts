import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {ScriptHackComponent} from '../../simple-elements/script/script.component';

@Component({
  selector: 'app-banner-adfox',
  templateUrl: './banner-adfox.component.html',
  styleUrls: ['./banner-adfox.component.less'],
})
export class BannerAdfoxComponent extends ScriptHackComponent implements OnInit {

  @Input()
  id: string;

  @Input()
  ownerId: string = '227654';

  @Input()
  params: {p1: string, p2: string};

  ngOnInit() {
    this.id = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    this.inner_script = `
        window.Ya.adfoxCode.create({
          ownerId: ${this.ownerId},
          containerId: 'adfox_${this.id}',
          params: {
            p1: '${this.params.p1}',
            p2: '${this.params.p2}'
          }
        });`;
  }

}
