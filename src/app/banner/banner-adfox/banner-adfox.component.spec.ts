import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerAdfoxComponent } from './banner-adfox.component';

describe('BannerAdfoxComponent', () => {
  let component: BannerAdfoxComponent;
  let fixture: ComponentFixture<BannerAdfoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BannerAdfoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerAdfoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
