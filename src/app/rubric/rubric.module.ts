import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {RubricMenuComponent} from './menu/rubric-menu.component';
import {TagModule} from '../tag/tag.module';
import {RubricService} from './rubric.service';
import {MenuModule} from '../menu/menu.module';
import {StaticLinksModule} from '../static-links/static-links.module';

@NgModule({
  declarations: [
    RubricMenuComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    TagModule,
    RouterModule,
    StaticLinksModule,
  ],
  exports: [
    RubricMenuComponent,
  ],
  providers: [
    RubricService,
  ]
})
export class RubricModule { }
