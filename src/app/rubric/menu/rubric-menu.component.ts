import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {IRubric, Rubric} from '../rubric';
import {RubricService} from '../rubric.service';
import {Response} from '@angular/http';
import {ActivatedRoute} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';

@Component({
  moduleId: module.id,
  selector: 'app-rubric-menu',
  providers: [RubricService],
  templateUrl: './rubric-menu.component.html',
  styleUrls: [
    './rubric-menu.component.less',
  ]
})
export class RubricMenuComponent implements OnInit, OnDestroy {

  @Input()
  show_hot_links: boolean = false;

  @Output()
  onRubricChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

  public rubrics: Rubric[] = [];
  public current_rubric: Rubric;

  public constructor(private rubric_service: RubricService,
                     private route: ActivatedRoute,
                     private metaService: Meta,
                     private titleService: Title) {}

  ngOnInit() {
    const rubric_observe = this.rubric_service.getList();
    rubric_observe.subscribe(
      (data) => {
        const meta = data['meta'];
        const objects = data['objects'];
        for (let obj_i in objects) {
          this.rubrics.push(new Rubric(objects[obj_i]));
        }
      }
    );
  }

  ngOnDestroy() {}

  changeRubric() {
    this.onRubricChanged.emit(true);
  }
}
