import {ITag, Tag} from '../tag/tag';
import {IMetaObject, MetaObject} from '../--classes/meta-object';

export interface IRubric extends IMetaObject {
  slug: string;
  title: string;
  primary: boolean;
  tags: ITag[];
}

export class Rubric extends MetaObject implements IRubric {
  slug: string;
  title: string;
  primary: boolean;
  tags: Tag[] = [];

  constructor(obj: IRubric) {
    super(obj);
    this.slug = obj.slug;
    this.title = obj.title;
    this.primary = obj.primary;
    for (let tag_i in obj.tags) {
      this.tags.push(new Tag(obj.tags[tag_i]));
    }

  }
}

