import {Injectable} from '@angular/core';
import {ApiListService} from '../--abstract/--api-list-service';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class RubricService extends ApiListService {

  public get_service_name() {
    return 'rubric';
  }

  public getList() {
    return this._getList();
  }

  public getDetail(slug) {
    return this._getDetail(slug);
  }

}
