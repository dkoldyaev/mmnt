import {Pipe, PipeTransform, Sanitizer} from '@angular/core';

@Pipe({
  name: 'safeHtml'
})
export class SafeHtmlPipe {

  constructor(private sanitizer: Sanitizer) {}

  transform(html) {
    return html;
    // return this.sanitizer.bypassSecurityTrustStyle(style);
    // return this.sanitizer.bypassSecurityTrustHtml(html);
    // return this.sanitizer.bypassSecurityTrustXxx(style); - see docs
  }
}
